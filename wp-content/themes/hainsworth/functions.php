<?php

/**
 * Serve up a new Hainsworth!
 *
 * @package hainsworth
 * @since   1.0.0
 */

/**
 * Assign the theme version to a var
 */
$theme              = wp_get_theme( 'hainsworth' );
$theme_version      = $theme['Version'];

require 'inc/hainsworth-template-tags.php';
require 'inc/hainsworth-template-hooks.php';
require 'inc/hainsworth-template-functions.php';
require 'inc/hainsworth-functions.php';

require 'inc/class-hainsworth-nav-walker.php';
require 'inc/class-hainsworth-templates.php';
require 'inc/class-hainsworth-customizer.php';
require 'inc/class-hainsworth-metaboxes.php';
require 'inc/class-hainsworth.php';

$startup = new Hainsworth();
$startup->init();

$templates = new Hainsworth_Templates();
$templates->templates_init();

$customizer = new Hainsworth_Customizer();
$customizer->customizer_init();

$metaboxes = new Hainsworth_Metaboxes();
$metaboxes->metaboxes_init();