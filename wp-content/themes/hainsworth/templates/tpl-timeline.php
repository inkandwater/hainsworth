<?php

/**
 * Template Name: Timeline
 *
 * @package hainsworth
 * @since   1.0.0
 */

get_header(); ?>

    <!-- content-area -->
    <section class="content-area">

        <?php while ( have_posts() ) : the_post();

            /**
             * Functions hooked into hainsworth_page_before
             *
             */
            do_action( 'hainsworth_page_before' );

            get_template_part( 'template-parts/content', 'page' );

            /**
             * Functions hooked into hainsworth_page_after
             *
             */
            do_action( 'hainsworth_page_after' );

        endwhile; ?>

    </section>

<?php
get_footer();