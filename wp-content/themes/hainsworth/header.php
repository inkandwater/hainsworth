<?php

/**
 * The header for our theme.
 *
 * @package hainsworth
 * @since   1.0.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<!-- head -->
<head>

    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">

    <?php wp_head(); ?>

</head>
<!-- /head -->

<!-- body -->
<body <?php body_class(); ?>>

    <?php
    /**
     * Functions hooked into hainsworth_site_before
     *
     * @see 10 hainsworth_off_canvas
     */
    do_action( 'hainsworth_site_before' ); ?>

    <div id="page" class="hfeed site">
        <?php
        /**
         * Functions hooked into hainsworth_header_before
         *
         */
        do_action( 'hainsworth_header_before' ); ?>

        <!-- header -->
        <header id="site-header" class="site-header">
            <div class="content-wrapper">
                <?php
                /**
                 * Functions hooked into hainsworth_header
                 *
                 * @see 10 hainsworth_menu_toggle
                 * @see 20 hainsworth_site_branding
                 * @see 30 hainsworth_main_navigation
                 */
                do_action( 'hainsworth_header' ); ?>
            </div>
        </header>
        <!-- /header -->

        <?php
        /**
         * Functions hooked into hainsworth_content_before
         *
         */
        do_action( 'hainsworth_content_before' ); ?>

        <!-- site-content -->
        <div class="site-content">

            <?php
            /**
             * Functions hooked into hainsworth_content_top
             *
             */
            do_action( 'hainsworth_content_top' );?>