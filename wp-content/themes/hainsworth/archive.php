<?php

/**
 * The template for displaying archive pages.
 *
 * @package hainsworth
 * @since   1.0.0
 */

get_header(); ?>

    <!-- content-area -->
    <section class="content-area">

            <?php if ( have_posts() ) : ?>

                <?php get_template_part( 'loop' );

            else :

                get_template_part( 'template-parts/content', 'none' );

            endif; ?>

    </section>

<?php
get_sidebar();
get_footer();
