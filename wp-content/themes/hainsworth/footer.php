<?php

/**
 * The template for displaying the footer.
 *
 * @package hainsworth
 * @since   1.0.0
 */

/**
 * Functions hooked into hainsworth_content_bottom
 *
 */
do_action( 'hainsworth_content_bottom' ); ?>

</div><!-- /site-content -->

    <?php
    /**
     * Functions hooked into hainsworth_footer_before
     *
     * @see 10 hainsworth_footer_bar
     */
    do_action( 'hainsworth_footer_before' ); ?>

    <!-- footer -->
    <footer id="footer" class="footer">
        <?php
        /**
         * Functions hooked into hainsworth_footer
         *
         * @see 10 hainsworth_footer_widgets
         * @see 20 hainsworth_site_terms
         */
        do_action( 'hainsworth_footer' ); ?>
    </footer>
    <!-- /footer -->

    <?php
    /**
     * Functions hooked into hainsworth_footer_before
     *
     * @see 10 hainsworth_off_canvas_close
     */
    do_action( 'hainsworth_footer_after' ); ?>

</div>

<!-- scripts -->
<?php wp_footer(); ?>
<!-- /scripts -->

</body>
</html>