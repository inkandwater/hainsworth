'use strict';

/* --------------------------
# Dependencies
---------------------------*/

var gulp         = require( "gulp" ),
    sass         = require( "gulp-sass" ),
    autoprefixer  = require( "autoprefixer" ),
    notify       = require( "gulp-notify" ),
    concat       = require( "gulp-concat" ),
    rename       = require( "gulp-rename" ),
    uglify       = require( "gulp-uglify" ),
    sourcemaps   = require( "gulp-sourcemaps" ),
    babel        = require( "gulp-babel" ),
    merge        = require( "merge-stream" ),
    postcss      = require( 'gulp-postcss' );

/* --------------------------
# Configuration
---------------------------*/

var config = {
    sassPath: './assets/sass',
    npmDir: './node_modules',
    jsPath: './assets/js',
}

/* --------------------------
# Sass Compile
---------------------------*/

gulp.task('styles', () => {

    var main = gulp.src( './assets/sass/style.scss' )
      .pipe( sourcemaps.init() )
      .pipe( sass({
          outputStyle: 'compressed',
          includePaths: [
             config.sassPath,
              config.npmDir + '/@fortawesome/fontawesome-free/scss',
              config.npmDir + '/bourbon/app/assets/stylesheets',
              config.npmDir + '/bourbon-neat/core',
              config.npmDir + '/modularscale-sass/stylesheets',
              config.npmDir + '/slick-carousel/slick',
            ]
          })
      .on("error", notify.onError(function (error) {
        return "Error: " + error.message;
      })))
      .pipe( postcss([ autoprefixer( {grid: true} ) ]) )
      .pipe( sourcemaps.write('./maps') )
      .pipe( gulp.dest('.') );

    var forms = gulp.src( './assets/sass/ninja-forms-custom.scss' )
      .pipe( sass({
          outputStyle: 'compressed',
          includePaths: [
            config.sassPath,
            config.npmDir + '/bourbon/app/assets/stylesheets',
            config.npmDir + '/bourbon-neat/core',
            config.npmDir + '/modularscale-sass/stylesheets',
          ]
      })
      .on("error", notify.onError(function (error) {
        return "Error: " + error.message;
      })))
      .pipe( postcss([ autoprefixer( {grid: true} ) ]) )
      .pipe( rename( 'ninja-forms.css' ) )
      .pipe( gulp.dest('./assets/css/') );

    var blocks = gulp.src( './assets/sass/blocks.scss' )
      .pipe( sass({
        outputStyle: 'compressed',
        includePaths: [
          config.sassPath,
          config.npmDir + '/bourbon-neat/core',
          config.npmDir + '/modularscale-sass/stylesheets',
        ]
      }))
      .on("error", notify.onError(function (error) {
        return "Error: " + error.message;
      }))
      .pipe( rename( 'blocks.min.css' ) )
      .pipe( postcss([ autoprefixer( {grid: true} ) ]) )
      .pipe( gulp.dest( './assets/css/' ) );

    return merge( main, forms, blocks );

});

/* --------------------------
# Icons Compile
---------------------------*/

gulp.task( 'icons', () => {
    return gulp.src( config.npmDir + '/@fortawesome/fontawesome-free/webfonts/**.*' )
      .pipe( gulp.dest('./assets/fonts/fontawesome') );
});

gulp.task( 'fonts', () => {
    return gulp.src( config.npmDir + '/slick-carousel/slick/fonts/**.*' )
      .pipe( gulp.dest('./assets/fonts/slick') );
});

gulp.task( 'loader', () => {
    return gulp.src( config.npmDir + '/slick-carousel/slick/ajax-loader.gif' )
      .pipe( gulp.dest('./assets/images') );
});

/* --------------------------
# Scripts Compile
---------------------------*/

gulp.task( 'main', () => {

    var jsScripts = [
        config.jsPath + '/src/main-menu.js',
        config.jsPath + '/src/dropdown-menus.js',
        config.jsPath + '/src/main.js',
        config.jsPath + '/src/animations.js',
        config.jsPath + '/src/sharing.js'
    ];

    return gulp.src( jsScripts )
        .pipe( concat( 'main.min.js' ) )
        .pipe( uglify() )
        .pipe( gulp.dest( config.jsPath + '/dist' ) )
        .pipe( notify( { message: 'Finished minifying Menu JavaScript'} ) );

});

gulp.task( 'carousel', () => {

    var jsScripts = [
        config.npmDir + '/slick-carousel/slick/slick.min.js',
        config.jsPath + '/src/carousel.js'
    ];

    return gulp.src( jsScripts )
        .pipe( concat( 'carousel.min.js' ) )
        .pipe( uglify() )
        .pipe( gulp.dest( config.jsPath + '/dist' ) )
        .pipe( notify( { message: 'Finished minifying Carousel JavaScript'} ) );

});

gulp.task( 'cookies', () => {

    var jsScripts = [
        config.jsPath + '/src/cookies.js'
    ];

    return gulp.src( jsScripts )
        .pipe( concat( 'cookies.min.js' ) )
        .pipe( uglify() )
        .pipe( gulp.dest( config.jsPath + '/dist' ) )
        .pipe( notify( { message: 'Finished minifying Cookies JavaScript'} ) );

});

/* --------------------------
# Watch Tasks
---------------------------*/

gulp.task('watch', () => {

    // Watch the input folder for change,
    // and run `sass` task when something happens
    gulp.watch( config.sassPath + '/**/*.scss' )

        .on('change', gulp.series('styles'));

        // // When there is a change,
        // // log a message in the console
        // .on('change', function( path, stats ) {
        //   console.log( 'File ' + path + ' was ' + event.type + ', running tasks...' );
        // });

});

/* --------------------------
# Dependencies
---------------------------*/

gulp.task( 'build', gulp.series([ 'styles', 'icons', 'fonts', 'loader', 'main', 'carousel', 'cookies' ]) );
gulp.task( 'default', gulp.series([ 'styles','watch' ]) );