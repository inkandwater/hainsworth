<?php

/**
 * The template for displaying search results pages.
 *
 * @package hainsworth
 * @since   1.0.0
 */
get_header(); ?>

    <!-- content-area -->
    <section class="content-area">

        <!-- search-results -->
        <div class="search-results">

            <?php
            /**
             * Functions hooked into hainsworth_search_results_content
             *
             */
            do_action( 'hainsworth_search_results_content' ); ?>

        </div><!-- /search-results -->

    </section>

<?php get_footer();