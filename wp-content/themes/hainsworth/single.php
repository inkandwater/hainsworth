<?php

/**
 * The template for displaying all single posts.
 *
 * @package hainsworth
 * @since   1.0.0
 */

get_header(); ?>

    <!-- content-area -->
    <section class="content-area">

        <?php while ( have_posts() ) : the_post();

            /**
             * Functions hooked into hainsworth_single_before
             *
             */
            do_action( 'hainsworth_single_before' );

            get_template_part( 'template-parts/content', 'single' );

            /**
             * Functions hooked into hainsworth_single_after
             *
             * @see 10 hainsworth_post_nav
             */
            do_action( 'hainsworth_single_after' );

        endwhile; ?>

    </section>

<?php
get_footer();