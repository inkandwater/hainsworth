<?php

/**
 * The template for displaying 404 pages (not found).
 *
 * @package hainsworth
 * @since   1.0.0
 */

get_header(); ?>

    <!-- content-area -->
    <section class="content-area">

            <!-- error-404 -->
            <div class="error-404 not-found">

                <?php
                /**
                 * @see 10 hainsworth_404_header
                 * @see 20 hainsworth_404_text
                 * @see 30 hainsworth_site_search
                 */
                do_action( 'hainsworth_404_page_content' ); ?>

            </div><!-- /error-404 -->

    </section>

<?php get_footer();