<?php

/**
 * Hainsworth template functions.
 *
 * @package hainsworth
 * @since   1.0.0
 */
namespace hainsworth;

/**
 * Header Functions.
 *
 * @package hainsworth
 * @since   1.0.0
 * @see     hainsworth_site_before
 * @see     hainsworth_header_before
 * @see     hainsworth_header
 * @see     hainsworth_content_before
 * @see     hainsworth_content_top
 */
if ( ! function_exists( __NAMESPACE__ . '/site_branding' ) ) :
    /**
     * Site branding wrapper and display
     */
    function site_branding() {
        ?>

        <!-- site-branding -->
        <div class="site-branding site-branding--header">

            <a class="site-branding__logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                <span class="logo"><?php include( get_theme_file_path( '/assets/images/hainsworth-protective-fabrics.svg' ) ); ?></span>
            </a>

        </div>
        <!-- /site-branding -->

        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '/menu_toggle' ) ) :
    /**
     * Menu Toggle button for small screens
     *
     */
    function menu_toggle() { ?>

        <div class="menu-toggle">
            <a id="menu-toggle" class="menu-toggle__button" aria-controls="site-navigation">
                <?php include( get_theme_file_path( '/assets/images/menu.svg' ) ); ?>
                <?php include( get_theme_file_path( '/assets/images/close.svg' ) ); ?>
            </a>
        </div>

    <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '/main_navigation' ) ) :
    /**
     * Site branding wrapper and display
     */
    function main_navigation() {

        if( has_nav_menu( 'main-menu' ) ) : ?>

            <!-- main-navigation -->
            <nav id="main-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_html_e( 'Primary Navigation', 'hainsworth' ); ?>">

                <?php main_menu(); ?>

            </nav>
            <!-- /main-navigation -->

        <?php endif;

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '/product_navigation' ) ) :
    /**
     * Site branding wrapper and display
     */
    function product_navigation() {

        if( has_nav_menu( 'product-menu' ) ) : ?>

            <!-- main-navigation -->
            <nav id="product-navigation" class="product-navigation" role="navigation" aria-label="<?php esc_html_e( 'Product Navigation', 'hainsworth' ); ?>">

                <?php product_menu(); ?>

            </nav>
            <!-- /main-navigation -->

        <?php endif;

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '/cookies_bar' ) ) :

    function cookies_bar() {

        $show    = get_theme_mod( 'hainsworth_cookies_show' );
        $content = get_theme_mod( 'hainsworth_cookies_content' );
        $link = get_theme_mod( 'hainsworth_cookies_link' );

        if ( true == $show && !empty( ( $content && $link ) ) ) : ?>

            <div id="cookie-bar" class="cookies-bar cookie-set">

                <div class="content-wrapper">
                    <?php printf( '<span class="cookies__content">%s<a class="cookies__link" href="%s">%s</a></span>',
                        esc_html( $content ),
                        esc_url( get_permalink( $link ) ),
                        __( 'Find out more.', 'hainsworth' )
                    ); ?>
                    <a id="close-cookie-bar" class="close-cookies-bar" href="#">
                        <?php include( get_theme_file_path( '/assets/images/close.svg' ) ); ?>
                    </a>
                </div>

            </div>

        <?php endif;

    }

endif;

/**
 * Footer Functions.
 *
 * @package hainsworth
 * @since   1.0.0
 * @see     hainsworth_content_bottom
 * @see     hainsworth_footer_before
 * @see     hainsworth_footer
 * @see     hainsworth_footer_after
 */
if ( ! function_exists( __NAMESPACE__ . '/footer_bar' ) ) :

    function footer_bar() {

        if( is_active_sidebar( 'footer-bar' ) ) :
            ?>

            <!-- footer-bar -->
            <aside class="footer-bar">
                <div class="content-wrapper">
                    <?php dynamic_sidebar( 'footer-bar' ); ?>
                </div>
            </aside>
            <!-- /footer-bar -->

        <?php

        endif;

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '/footer_bottom_bar' ) ) :

    function footer_bottom_bar() {
    ?>

        <div class="footer__bottom-bar">
            <div class="content-wrapper">

                <?php site_branding_footer_aw(); ?>
                <?php site_copyright_link(); ?>
                <?php site_social(); ?>

            </div>
        </div>

    <?php }

endif;

if ( ! function_exists( __NAMESPACE__ . '/site_branding_footer' ) ) :

    function site_branding_footer() {
        ?>

        <!-- site-branding -->
        <div class="site-branding site-branding--footer">
            <a class="site-branding__logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                <span class="logo"><?php include( get_theme_file_path( '/assets/images/hainsworth-protective-fabrics.svg' ) ); ?></span>
            </a>
        </div>
        <!-- /site-branding -->

        <?php

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '/site_terms' ) ) :

    function site_terms() {
        ?>

        <div class="site-bottom-bar">

        <!-- site-terms -->
        <div class="site-terms">
            <?php

            $defaults = array(
                    'theme_location' => 'footer-menu',
                    'container'      => false,
                    'fallback_cb'    => false,
                    'echo'           => true,
                    'depth'          => 2,
                    'menu_class'     => 'site-copyright',
                    'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            );

            wp_nav_menu( apply_filters( 'hainsworth_footer_menu_args', $defaults ) ); ?>

        </div>
        <!-- /site-terms -->

        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '/site_social' ) ) :

    function site_social() {

        $social_options = social_media_get_links();

        if ( $social_options ) : ?>

            <!-- site-social -->
            <div class="site-social">
                <?php social_links(); ?>
            </div>
            <!-- /site-social -->

        <?php endif;

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '/footer_widgets' ) ) :

    function footer_widgets() {
    ?>

        <!-- footer-widgets  -->
        <?php if( is_active_sidebar( 'footer-widget-area' ) ) : ?>

            <aside class="footer-widget-area">
                <?php dynamic_sidebar( 'footer-widget-area' ); ?>
            </aside>

        <?php endif; ?>
        <!-- /footer-widgets  -->

    <?php }

endif;

if ( ! function_exists( __NAMESPACE__ . '/footer_container_open' ) ) :

    function footer_container_open() { ?>

        <div class="site-footer__inner content-wrapper">

    <?php }

endif;

if ( ! function_exists( __NAMESPACE__ . '/footer_container_close' ) ) :

    function footer_container_close() { ?>

        </div>

    <?php }

endif;

if ( ! function_exists( __NAMESPACE__ . '/site_map' ) ) :

    function site_map() {
        ?>

        <!-- site-map -->
        <div class="site-map">

            <?php site_map_menu(); ?>

        </div>
        <!-- site-map -->

        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '/site_copyright_link' ) ) :

    function site_copyright_link() { ?>

        <ul class="site-copyright">
            <?php printf( '<li><span class="site-copyright__copy">%s</span><a href="%s" rel="index">%s %s.</a></li>',
                __( '&copy; ', 'hainsworth' ),
                esc_url( home_url() ),
                get_bloginfo( 'name' ),
                date('Y')
            ); ?>

            <?php printf( '<li><span class="site-copyright__designed">%s</span><a href="%s" target="_blank">%s</a></li>',
                __( 'Designed by ', 'hainsworth' ),
                esc_url( 'https://www.thebackroom.agency' ),
                __( 'thebackroom.agency', 'hainsworth' )
            ); ?>
        </ul>

    <?php }

endif;

if ( ! function_exists( __NAMESPACE__ . '/site_branding_footer_aw' ) ) :

    function site_branding_footer_aw() {
        ?>

        <!-- site-branding -->
        <div class="site-branding site-branding--footer-aw">

            <div class="branding-container">
                <?php printf( '<span class="pre-logo">%s</span>', __( 'Part of', 'hainsworth' ) ) ?>
                <span class="logo"><?php include( get_theme_file_path( '/assets/images/aw-logo.svg' ) ); ?></span>
            </div>

        </div>
        <!-- /site-branding -->

        <?php

    }

endif;

/**
 * Post Functions.
 *
 * @package hainsworth
 * @since   1.0.0
 * @see     hainsworth_loop_before
 * @see     hainsworth_loop_after
 * @see     hainsworth_single_before
 * @see     hainsworth_single_post_top
 * @see     hainsworth_single_post
 * @see     hainsworth_single_post_bottom
 * @see     hainsworth_single_after
 */
if ( ! function_exists( __NAMESPACE__ . '/display_comments' ) ) :
    /**
     * Hainsworth display comments
     *
     * @since  1.0.0
     */
    function display_comments() {

        // If comments are open or we have at least one comment, load up the comment template.
        if ( comments_open() || '0' != get_comments_number() ) :
            comments_template();
        endif;

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '/post_header' ) ) :
    /**
     * Display the post header with a link to the single post
     *
     * @since 1.0.0
     */
    function post_header() {
        ?>

        <!-- entry-header -->
        <header class="post__header entry-header">
            <?php
            if ( is_single() ) {
                // category_list();
                the_title( '<h1 class="post__title entry-title">', '</h1>' );
                posted_on();
            } else {
                posted_on();
                the_title( sprintf( '<h2 class="post__title entry-title h4"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
                if ( 'post' == get_post_type() ) { ?>
                    <?php category_list(); ?>
                <?php }
            }
            ?>
        </header>
        <!-- /entry-header -->

        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '/post_thumbnail' ) ) :
    /**
     * Display post thumbnail
     *
     * @var $size thumbnail size. thumbnail|medium|large|full|$custom
     * @uses has_post_thumbnail()
     * @uses the_post_thumbnail
     * @param string $size the post thumbnail size.
     * @since 1.5.0
     */
    function post_thumbnail( $size = 'full' ) {

        $size = apply_filters( 'hainsworth_post_thumbnail_size', $size );

        if ( has_post_thumbnail() ) { ?>

            <!-- entry-thumbnail -->
            <div class="post__thumbnail entry-thumbnail">
                <?php the_post_thumbnail( $size ); ?>
            </div>
            <!-- /entry-thumnbail -->

        <?php }
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '/post_content' ) ) :
    /**
     * Display the post content with a link to the single post
     *
     * @since 1.0.0
     */
    function post_content() {
        $class = ( is_single() )? 'entry-content' : 'entry-summary';
        ?>
            <!-- entry-content -->
            <div class="post__content <?php echo esc_attr( $class ); ?> content-block">
                <?php

                /**
                 * Functions hooked in to hainsworth_post_content_before action.
                 *
                 * @hooked hainsworth_post_thumbnail - 10
                 */
                do_action( 'hainsworth_post_content_before' );

                if( is_single() ) :
                    the_content(
                        sprintf(
                            __( 'Continue reading %s', 'hainsworth' ),
                            '<span class="screen-reader-text">' . get_the_title() . '</span>'
                        )
                    );
                else :
                    the_excerpt();
                endif;

                do_action( 'hainsworth_post_content_after' );

                wp_link_pages( array(
                    'before' => '<div class="page-links">' . __( 'Pages:', 'hainsworth' ),
                    'after'  => '</div>',
                ) );
                ?>
            </div>
            <!-- /entry-content -->
        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '/paging_nav' ) ) :
    /**
     * Display navigation to next/previous set of posts when applicable.
     */
    function paging_nav() {
        global $wp_query;

        $args = array(
            'type'      => 'list',
            'next_text' => _x( 'Next', 'Next post', 'hainsworth' ),
            'prev_text' => _x( 'Previous', 'Previous post', 'hainsworth' ),
            );

        the_posts_pagination( $args );
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '/post_nav' ) ) :
    /**
     * Display navigation to next/previous post when applicable.
     */
    function post_nav() {

        $args = array(
            'prev_text' => __( '<span class="nav-arrow h6">Previous</span> <span class="nav-previous-title h5">%title</span>', 'hainsworth' ),
            'next_text' => __( '<span class="nav-arrow h6">Next</span> <span class="nav-next-title h5">%title</span>', 'hainsworth' ),
            );
        ?>

        <!-- post-navigation -->
        <?php the_post_navigation( $args ); ?>
        <!-- /post-navigation -->

        <?php

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '/category_list' ) ) :

    function category_list() {

        /* translators: used between list items, there is a space after the comma */
        $categories_list = get_the_category_list( esc_html__( ', ', 'hainsworth' ) );

        if ( $categories_list ) :
            printf( '<div class="categories-links h6">%s</div>',
                wp_kses_post( $categories_list )
            );
        endif; // End if $tags_list.

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '/tag_list' ) ) :

    function tag_list() {

        /* translators: used between list items, there is a space after the comma */
        $tags_list = get_the_tag_list( '', __( ', ', 'hainsworth' ) );

        if ( $tags_list ) :
            printf( '<div class="tags-links"><span class="tags-label">%s</span> %s</div>',
                esc_attr( __( 'Tagged', 'hainsworth' ) ),
                wp_kses_post( $tags_list )
            );
        endif; // End if $tags_list.

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '/posted_on' ) ) :
    /**
     * Prints HTML with meta information for the current post-date/time and author.
     */
    function posted_on() {

        $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
        if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
            $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time> <time class="updated" datetime="%3$s">%4$s</time>';
        }

        $time_string = sprintf( $time_string,
            esc_attr( get_the_date( 'c' ) ),
            esc_html( get_the_date( 'jS F Y' ) ),
            esc_attr( get_the_modified_date( 'c' ) ),
            esc_html( get_the_modified_date() )
        );

        $posted_on = sprintf( '<a href="%1s" rel="bookmark">%2s</a>', esc_url( get_permalink() ), $time_string );

        echo wp_kses( apply_filters( 'hainsworth_single_post_posted_on_html', '<div class="posted-on h6">' . $posted_on . '</div>', $posted_on ), array(
            'div' => array(
                'class'  => array(),
            ),
            'a'    => array(
                'href'  => array(),
                'title' => array(),
                'rel'   => array(),
            ),
            'time' => array(
                'datetime' => array(),
                'class'    => array(),
            ),
        ) );
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '/posted_by' ) ) :

    function posted_by() {
        ?>
            <div class="author-meta">
                <?php printf( '<div class="avatar">%s</div>', get_avatar( get_the_author_meta( 'ID' ), 48 ) ); ?>
                <?php printf( '<b class="label">%s</b>', get_the_author_posts_link() ); ?>
            </div>

        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '/news_posts_open' ) ) :

    function news_posts_open() { ?>

        <div class="news-posts alignwide">

    <?php }

endif;

if ( ! function_exists( __NAMESPACE__ . '/news_posts_close' ) ) :

    function news_posts_close() { ?>

        </div>

    <?php }

endif;

/**
 * Page Functions.
 *
 * @package hainsworth
 * @since   1.0.0
 * @see     hainsworth_page_before
 * @see     hainsworth_page_after
 */
if ( ! function_exists( __NAMESPACE__ . '/page_header' ) ) :

    function page_header( $size = 'full' ) {

        $size = apply_filters( 'hainsworth_page_header_thumbnail_size', $size );
        ?>

        <!-- entry-header -->
        <header class="page__header entry-header">

            <?php if( has_post_thumbnail() ) :
                the_post_thumbnail( $size );
            endif; ?>

            <?php the_title( '<h1 class="page__title entry-title">', '</h1>' ); ?>

        </header>
        <!-- /entry-header -->

        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '/page_content' ) ) :

    function page_content() { ?>

        <!-- entry-content -->
        <div class="page__content entry-content content-block">
            <?php the_content(); ?>
            <?php
                wp_link_pages( array(
                    'before' => '<div class="page-links">' . __( 'Pages:', 'hainsworth' ),
                    'after'  => '</div>',
                ) );
            ?>
        </div>
        <!-- /entry-content -->

        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '/news_content' ) ) :

    function news_content() {

        $page_ID = get_option( 'page_for_posts' );
        $post    = get_post( $page_ID );

        if ( $page_ID && !empty( $post ) ) {

            setup_postdata( $post ); ?>

            <div class="page__content entry-content content-block">
                <?php the_content(); ?>
            </div>

            <?php wp_reset_postdata();

        }

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '/get_sidebar' ) ) :
    /**
     * Display hainsworth sidebar
     *
     * @uses get_sidebar()
     * @since 1.0.0
     */
    function get_sidebar() {

        get_sidebar();

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '/page_content_wrapper' ) ) :

    function page_content_wrapper() { ?>

        <div class="page-content__wrapper">

    <?php }

endif;

if ( ! function_exists( __NAMESPACE__ . '/div_close' ) ) :

    function div_close() { ?>

        </div>

    <?php }

endif;

if ( ! function_exists( __NAMESPACE__ . '/header_block' ) ) :

    function header_block() {

        $slides = get_post_meta( get_the_ID(), '_hainsworth_header_block_slides', true );

        if ( isset( $slides[0]['slide_background_image'] ) ) : ?>

            <?php $slider_init = ( count( $slides ) != 1 ) ? 'js-slider-init' : null; ?>

            <section id="header-block" class="header-block" aria-label="Header Block">

                <?php do_action( 'hainsworth_before_header_block' ); ?>

                <div class="content-wrapper">

                    <div class="header-block-slides <?php echo $slider_init; ?>">

                        <?php foreach( $slides as $slide ) {

                            $bg_image  = ( $slide['slide_background_image'] )? $slide['slide_background_image'] : null;
                            $alignment = ( $slide['slide_alignment'] )? $slide['slide_alignment'] : null;
                            $content     = ( $slide['slide_title'] )? $slide['slide_title'] : null;
                            ?>

                            <div class="header-block__slide animate animate--is-visible <?php echo esc_attr( $alignment ); ?>" style="background-image: url( <?php echo esc_url( $bg_image ) ?> );">

                                    <?php printf( '<div class="header-block__slide--content">%s</div>', wp_kses_post( $content ) ); ?>

                            </div>

                        <?php } ?>

                    </div>

                </div>

            </section>

        <?php endif; ?>

    <?php }

endif;

/**
 * Homepage Functions.
 *
 * @package hainsworth
 * @since   1.0.0
 *
 */
if ( ! function_exists( __NAMESPACE__ . '/homepage_content' ) ) :

    function homepage_content() {

        if( have_posts() ) :

            echo '<section class="homepage-content homepage-section" aria-label="Homepage Content">';

            while ( have_posts() ) : the_post();

                get_template_part( 'template-parts/content', 'page' );

            endwhile;

            wp_reset_postdata();

            echo '</section>';

        endif;

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '/homepage_posts' ) ) :

    function homepage_posts() {

        $args = array(
            'post_type'         => 'post',
            'posts_per_page'    => 5
        );

        $homepage_posts = new WP_Query( $args );

        if( $homepage_posts->have_posts() ) :

            echo '<section class="homepage-posts homepage-section" aria-label="Homepage Posts">';

            $title = sprintf( '<h2 class="section-title">%s</h2>', __( 'Posts', 'hainsworth' ) );
            $title = apply_filters( 'hainsworth_homepage_posts_section_title', $title );

            echo wp_kses_post( $title );

            while ( $homepage_posts->have_posts() ) : $homepage_posts->the_post();

                get_template_part( 'template-parts/content', get_post_format() );

            endwhile;

            wp_reset_postdata();

            echo '</section>';

        endif;

    }

endif;

/**
 * 404 Functions.
 *
 * @package hainsworth
 * @since   1.0.0
 * @see     hainsworth_404_page_content
 */
if ( ! function_exists( __NAMESPACE__ . '/error_404_header' ) ) :

    function error_404_header() {
        ?>
            <!-- page-header -->
            <header class="page-header">
                <h1 class="page-title"><?php esc_html_e( '404. Page Not Found', 'hainsworth' ); ?></h1>
            </header>
            <!-- /page-header -->
        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '/error_404_text' ) ) :

    function error_404_text() {
        ?>
            <!-- page-content -->
            <div class="page-content">
                <p><?php esc_html_e( 'There\'s no page at this location (either it\'s moved, or you typed in the URL incorrectly). To get back on track, try one of the links in the header or do a search for what you\'re after:', 'hainsworth' ); ?></p>
            </div>
            <!-- /page-content -->
        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '/site_search' ) ) :

    function site_search() {
        ?>
            <!-- site-search -->
            <section aria-label="Search">
                <?php get_search_form(); ?>
            </section>
            <!-- /site-search -->
        <?php
    }

endif;

/**
 * Custom Functions
 *
 * @package hainsworth
 * @since   1.0.0
 */
if ( ! function_exists( __NAMESPACE__ . '/timeline' ) ) :

    function timeline() {

        $entries = get_timeline_entries();

        if ( $entries->have_posts() ) : ?>

            <div class="timeline-container">

                <div class="timeline--start-year"><?php echo '1900'; ?></div>

                <div class="timeline-wrapper">

                    <?php global $counter; ?>
                    <?php $counter = 1 ?>

                    <?php while( $entries->have_posts() ) : $entries->the_post();

                        get_template_part( 'template-parts/content', 'timeline-entry' );

                        $counter ++;
                    endwhile; ?>

                </div>

                <div class="timeline--end-year"><?php echo date('Y'); ?></div>

            </div>

        <?php endif;

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '/sharing' ) ) :

    function sharing() {
        ?>

        <div class="sharing">
            <?php printf( '<strong class="sharing__title">%s</strong>', __( 'Share', 'hainsworth' ) ); ?>
            <ul class="sharing__list">
                <li class="sharing__item">
                    <?php printf( '<a class="sharing__link sharing__link--facebook js-share" href="https://www.facebook.com/sharer/sharer.php?u=%s"><i class="fab fa-facebook-square"></i></a>', urlencode( get_permalink() ) );?> </li>
                <li class="sharing__item">
                    <?php printf( '<a class="sharing__link sharing__link--twitter js-share" href="https://twitter.com/intent/tweet?url=%s&text=%s"><i class="fab fa-twitter"></i></a>',
                        urlencode( get_permalink() ),
                        urlencode( get_the_excerpt() )
                    ); ?>
                </li>
                <li class="sharing__item">
                    <?php printf( '<a class="sharing__link sharing__link--linkedin js-share" href="https://www.linkedin.com/shareArticle?mini=true&url=%s&title=%s&summary=%s&source=LinkedIn"><i class="fab fa-linkedin"></i></a>',
                        urlencode( get_permalink() ),
                        urlencode( get_the_title() ),
                        urlencode( get_the_excerpt() )
                    ); ?>
                </li>
            </ul>
        </div>

        <?php
    }

endif;