<?php

/**
 * Hainsworth hooks
 *
 * @package hainsworth
 * @since   1.0.0
 */

/**
 * General
 *
 */
add_action( 'hainsworth_sidebar',                   'hainsworth\\get_sidebar',            10 );

add_filter( 'body_class',					       'hainsworth\\custom_body_classes',    10, 1 );
add_filter( 'post_class',					       'hainsworth\\custom_post_classes',    10, 1 );
add_filter( 'hainsworth_post_thumbnail_size',      'hainsworth\\thumbnail_size',         10, 1 );

/**
 * Header
 *
 * @see hainsworth_header_before
 * @see hainsworth_header
 * @see hainsworth_content_top
 */
add_action( 'hainsworth_site_before',               'hainsworth\\cookies_bar',            10 );
add_action( 'hainsworth_header',                    'hainsworth\\menu_toggle',            10 );
add_action( 'hainsworth_header',                    'hainsworth\\site_branding',          20 );
add_action( 'hainsworth_header',                    'hainsworth\\main_navigation',        40 );

add_action( 'hainsworth_content_before',            'hainsworth\\product_navigation',     10 );


/**
 * Footer
 *
 * @see hainsworth_footer_before
 * @see hainsworth_footer
 * @see hainsworth_footer_after
 */
add_action( 'hainsworth_footer',                    'hainsworth\\footer_container_open',  10 );
add_action( 'hainsworth_footer',                    'hainsworth\\site_branding_footer',   20 );
add_action( 'hainsworth_footer',                    'hainsworth\\site_map',               30 );
add_action( 'hainsworth_footer',                    'hainsworth\\footer_widgets',         40 );
add_action( 'hainsworth_footer',                    'hainsworth\\footer_container_close', 80 );
add_action( 'hainsworth_footer',                    'hainsworth\\footer_bottom_bar',      90 );

/**
 * Pages
 *
 * @see hainsworth_page
 */
add_action( 'hainsworth_page',                      'hainsworth\\page_header',            10 );
add_action( 'hainsworth_page',                      'hainsworth\\header_block',           10 );
add_action( 'hainsworth_page',                      'hainsworth\\page_content_wrapper',   20 );
add_action( 'hainsworth_page',                      'hainsworth\\page_content',           30 );
add_action( 'hainsworth_page',                      'hainsworth\\div_close',              40 );

/**
 * Posts
 *
 * @see hainsworth_loop_before
 * @see hainsworth_loop_post
 * @see hainsworth_loop_after
 * @see hainsworth_single_post
 * @see hainsworth_single_after
 * @see hainsworth_single_post_bottom
 */
add_action( 'hainsworth_loop_before',               'hainsworth\\news_content',           10 );
add_action( 'hainsworth_loop_before',               'hainsworth\\news_posts_open',        20 );
add_action( 'hainsworth_loop_post',                 'hainsworth\\post_thumbnail',         10 );
add_action( 'hainsworth_loop_post',                 'hainsworth\\post_header',            20 );
add_action( 'hainsworth_loop_after',                'hainsworth\\news_posts_close',       10 );
add_action( 'hainsworth_loop_after',                'hainsworth\\paging_nav',             20 );
add_action( 'hainsworth_single_post',               'hainsworth\\post_header',            10 );
add_action( 'hainsworth_single_post',               'hainsworth\\post_thumbnail',         20 );
add_action( 'hainsworth_single_post',               'hainsworth\\post_content',           30 );
add_action( 'hainsworth_single_post',               'hainsworth\\sharing',                40 );
add_action( 'hainsworth_single_after',              'hainsworth\\post_nav',               10 );

/**
 * Homepage
 *
 * @see homepage
 */
add_action( 'homepage',                             'hainsworth\\header_block',           10 );
add_action( 'homepage',                             'hainsworth\\page_content_wrapper',   20 );
add_action( 'homepage',                             'hainsworth\\page_content',           30 );
add_action( 'homepage',                             'hainsworth\\div_close',              40 );

/**
 * 404
 *
 * @see hainsworth_404_page_content
 */
add_action( 'hainsworth_404_page_content',          'hainsworth\\404_header',             10 );
add_action( 'hainsworth_404_page_content',          'hainsworth\\404_text',               20 );
add_action( 'hainsworth_404_page_content',          'hainsworth\\site_search',            30 );