<?php

/**
 * Hainsworth Customizer Class
 *
 * @package hainsworth
 * @since   1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Hainsworth_Customizer' ) ) :

    /**
     * The Hainsworth Customizer class
     */
    class Hainsworth_Customizer {

        /**
         * The meta_prefix of the theme.
         *
         * @since    1.0.0
         * @access   protected
         * @var      string    $meta_prefix    The meta prefix of the theme.
         */
        protected $prefix;

        /**
         * The Text Domain of the theme.
         *
         * @since    1.0.0
         * @access   protected
         * @var      string    $text_domain    The Text Domain of the theme.
         */
        protected $text_domain;

        public function __construct() {

            $this->prefix       = 'hainsworth_';
            $this->text_domain = 'hainsworth';

        }
        public function customizer_init() {

            add_action( 'customize_register',   array( $this, 'edit_controls' ),    10 );
            add_action( 'customize_register',   array( $this, 'general_settings' ), 20 );
            add_action( 'customize_register',   array( $this, 'customize_register' ), 10 );
            add_action( 'customize_register',   array( $this, 'social' ), 10 );

            // Enqueue live preview javascript in Theme Customizer admin screen
            // add_action( 'customize_preview_init' , array( $this, 'live_preview' ) );

        }

        public function edit_controls( $wp_customize ) {

            $wp_customize->remove_section( 'background_image' );
            $wp_customize->remove_section( 'colors' );
            $wp_customize->remove_control( 'display_header_text' );
            $wp_customize->remove_control( 'custom_logo' );

            $wp_customize->get_control( 'blogname' )->priority          = 30;
            $wp_customize->get_control( 'blogdescription' )->priority   = 40;
            $wp_customize->get_control( 'site_icon' )->priority         = 50;

            $wp_customize->get_section( 'title_tagline' )->panel     = 'general_settings';

        }

        public function general_settings( $wp_customize ) {

            $wp_customize->add_panel( 'general_settings', array(
                'priority'       => 30,
                'capability'     => 'edit_theme_options',
                'theme_supports' => '',
                'title'          => __( 'General Settings', $this->text_domain ),
            ) );

        }

        public function customize_register( $wp_customize ) {

            // Footer Bar
            $wp_customize->add_section( 'hainsworth_footer_bar', array(
                'title'                 => __( 'Footer Bar' ),
                'priority'              => 100,
                'description'           => __( "The footer bar is a widget area that shows just above the footer.<br><br> It's best suited to having a single inline widget, such as a mail sign up form or call to action link. Once you've added a widget, you can customize the footer bar's appearance below.", 'hainsworth' )
            ) );

            // Background Color
            $wp_customize->add_setting( 'hainsworth_footer_bar_bg_color', array(
                'default'               => '',
                'sanitize_callback'     => 'sanitize_hex_color'
            ) );

            $wp_customize->add_control(
                new WP_Customize_Color_Control(
                    $wp_customize, 'hainsworth_footer_bar_bg_color', array(
                        'label'         => __( 'Background Colour', '' ),
                        'section'       => 'hainsworth_footer_bar',
                        'settings'      => 'hainsworth_footer_bar_bg_color',
                        'priority'      => 10
            ) ) );

            // Background Image
            $wp_customize->add_setting( 'hainsworth_footer_bar_bg_image', array(
                'default'               => '',
                'sanitize_callback'     => 'esc_url_raw'
            ) );

            $wp_customize->add_control(
                new WP_Customize_Upload_Control(
                    $wp_customize, 'hainsworth_footer_bar_bg_image', array(
                        'label'         => __( 'Background Image', '' ),
                        'section'       => 'hainsworth_footer_bar',
                        'settings'      => 'hainsworth_footer_bar_bg_image',
                        'priority'      => 10
            ) ) );

            $wp_customize->add_section( 'hainsworth_cookies', array(
                'title'                 => __( 'Cookies' ),
                'priority'              => 100,
                'description'           => __( "Adds a banner across the screen to alert about the use of cookies on the site.", 'hainsworth' )
            ) );

                $wp_customize->add_setting( 'hainsworth_cookies_show', array(
                    'default'               => '',
                ) );

                $wp_customize->add_control( 'hainsworth_cookies_show', array(
                    'label'             => __( 'Show Cookies?', 'hainsworth' ),
                    'description'       => __( 'Make the cookies bar visible', $this->text_domain ),
                    'section'           => 'hainsworth_cookies',
                    'type'              => 'checkbox',
                    'priority'          => 50,
                ) );

                $wp_customize->add_setting( 'hainsworth_cookies_content', array(
                    'default'               => '',
                    'sanitize_callback'     => 'esc_html'
                ) );

                $wp_customize->add_control( 'hainsworth_cookies_content', array(
                    'label'             => __( 'Cookie bar content', 'hainsworth' ),
                    'description'       => __( 'Add some content for the cookies bar', $this->text_domain ),
                    'section'           => 'hainsworth_cookies',
                    'type'              => 'textarea',
                    'priority'          => 50,
                ) );

                $wp_customize->add_setting( 'hainsworth_cookies_link', array(
                    'default'               => '',
                ) );

                $wp_customize->add_control( 'hainsworth_cookies_link', array(
                    'label'             => __( 'Cookie bar link', 'hainsworth' ),
                    'description'       => __( 'Add a link to the cookies page', $this->text_domain ),
                    'section'           => 'hainsworth_cookies',
                    'type'              => 'dropdown-pages',
                    'priority'          => 50,
                ) );

        }

        public function social( $wp_customize ) {

            $wp_customize->add_section( 'social', array(
                'title'                 => __( 'Social Media' ),
                'priority'              => 40,
                'panel'                 => 'general_settings'
            ) );

            $social_links = Hainsworth\social_media_get_links();

            foreach( $social_links as $key => $value ) {

                $wp_customize->add_setting( $value['slug'], array(
                    'sanitize_callback' => 'esc_url_raw',
                ) );

                $wp_customize->add_control( $value['slug'], array(
                    'label'             => $value['label'],
                    'description'       => __( 'Add the relevant link to this social media account', $this->text_domain ),
                    'section'           => 'social',
                    'type'              => 'text',
                    'priority'          => 50,
                ) );

            }

        }

    }

endif;