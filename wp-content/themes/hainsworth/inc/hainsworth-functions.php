<?php

/**
 * Hainsworth Functions
 *
 * @package hainsworth
 * @since   1.0.0
 */

namespace hainsworth;

if ( ! function_exists( __NAMESPACE__ . '\custom_body_classes' ) ) :

    /**
     *
     * @since  1.0.0
     */
    function custom_body_classes( $classes ) {

        $featured = (
            is_page() ||
            is_singular( 'post' )
        )? true : false;

        $classes[] = 'no-js';

        if( $featured && has_post_thumbnail() ) {
            $classes[] = 'has-featured-image';
        }

        return $classes;

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\custom_post_classes' ) ) :

    /**
     *
     * @since  2.1.0
     */
    function custom_post_classes( $classes ) {

        $classes = array();

        if( is_front_page() ) :
            $classes[] = 'home';
        endif;

        if( has_post_thumbnail() ) :
            $classes[] = 'has-featured-image';
        endif;

        if( 'post' == get_post_type() && !is_single() ) {
            $classes = array( 'animate', 'animate--up', 'post', 'card', 'card--post', 'hentry' );
            if( has_post_thumbnail() ) {
                $classes[] = 'post--has-featured-image';
            }
        } elseif( 'post' == get_post_type() && is_single() ) {
            $classes = array( 'post', 'post--single', 'hentry' );
            if( has_post_thumbnail() ) {
                $classes[] = 'post--has-featured-image';
            }
        }

        if( 'timeline' == get_post_type() && !is_single() ) {
            $classes = array( 'timeline-entry', 'animate', 'card', 'card--timeline-entry', 'hentry' );
            if( has_post_thumbnail() ) {
                $classes[] = 'timeline-entry--has-featured-image';
            }

            global $counter;
            $classes[] = ( $counter % 2 ) ? sprintf( 'align-left animate--right' ) : sprintf( 'align-right animate--left' );
        }

        array_push( $classes, 'hentry');
        array_push( $classes, get_post_type() );

        return $classes;

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\social_media_get_links' ) ) :

    function social_media_get_links() {

        $social_links = get_option( 'social_media_links' );
        $social_links = apply_filters( 'social_media_links', $social_links );

        return $social_links;

    }

endif;

if ( ! function_exists( __NAMESPACE__ . 'social_links' ) ) :

    function social_links() {

        $social_options = social_media_get_links(); ?>

        <ul class="social-links">

            <?php foreach ( $social_options as $slug => $array ) :

                $link  = get_theme_mod( $array['slug'] );

                if ( '' != $link ) :

                    printf( '<li class="social-links__item %1$s-slug">
                                <a class="social-links__link" href="%2$s" title="%3$s" target="_blank" rel="noopener">
                                    <i class="fa fas %4$s"></i>
                                </a>
                            </li>',
                        esc_attr( $slug ),
                        esc_url(  $link ),
                        esc_attr( $array['label'] ),
                        esc_attr( $array['class'] )
                    );

                endif;

            endforeach; ?>

        </ul>

    <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\get_wysiwyg_output' ) ) :
    /**
     *
     * @since  2.1.0
     */
    function get_wysiwyg_output( $meta_key, $post_id = 0 ) {

        global $wp_embed;

        $post_id = $post_id ? $post_id : get_the_id();
        $content = get_post_meta( $post_id, $meta_key, 1 );

        if ( ! $content ) {
            return '';
        }

        $content = $wp_embed->autoembed( $content );
        $content = $wp_embed->run_shortcode( $content );
        $content = wpautop( $content );
        $content = do_shortcode( $content );

        return $content;

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\oembed_wrapper' ) ) :

    /**
     * Add wrapper around oembeds
     *
     * @since  2.1.0
     */
    function oembed_wrapper( $html, $url, $attr, $post_id ) {

        // Set up an array for some classes.
        $classes = array();

        // Add some classes.
        $classes[] = 'responsive-embed';
        $classes[] = 'responsive-embed--widescreen';

        // Apply filters so we can easily change these classes.
        $classes = apply_filters( 'hainsworth_oembed_wrapper_classes', $classes );

        // If $classes is an array (which, if done right, it should be), implode into a string.
        if ( is_array( $classes ) ) :
            $classes = implode( ' ', $classes );
        endif;

        // Return the $html with a new wrapper div.
        return sprintf( '<div class="%1s">%2s</div>', esc_attr( $classes ), $html );
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\thumbnail_size' ) ) :

    function thumbnail_size( $size ) {

        if ( is_home() || is_archive() ) :
            $size = 'post';
        endif;

        return $size;

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\get_timeline_entries' ) ) :

    function get_timeline_entries() {

        $args = array(
            'post_type'      => 'timeline',
            'order'          => 'ASC',
            'orderby'        => 'title',
            'posts_per_page' => -1
        );

        $entries = new \WP_Query($args);

        if ( !empty( $entries ) )
            return $entries;

    }

endif;