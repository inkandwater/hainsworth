<?php

/**
 * Hainsworth Templates Class
 *
 * @package hainsworth
 * @since   1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Hainsworth_Templates' ) ) :

    /**
     * The Hainsworth Templates class
     */
    class Hainsworth_Templates {

        public function templates_init() {

            add_action( 'wp_head',      array( $this, 'homepage' ),     10 );
            add_action( 'wp_head',      array( $this, 'product' ),      10 );
            add_action( 'wp_head',      array( $this, 'full_width' ),   10 );
            add_action( 'wp_head',      array( $this, 'timeline' ),     10 );

        }

        public function homepage() {

            if ( is_page_template( 'templates/tpl-homepage.php' ) ) :

                remove_action( 'hainsworth_content_before',    'hainsworth\\product_navigation',     10 );

                add_action( 'hainsworth_before_header_block',    'hainsworth\\product_navigation',     10 );

            endif;

        }

        public function product() {

            if (
                is_page_template( 'templates/tpl-product-red.php' ) ||
                is_page_template( 'templates/tpl-product-green.php' ) ||
                is_page_template( 'templates/tpl-product-gold.php' ) ||
                is_page_template( 'templates/tpl-product-blue.php' )
            ) :

                remove_action( 'hainsworth_page',         'hainsworth\\page_header',         10 );

            endif;

        }

        public function full_width() {

            if ( is_page_template( 'templates/tpl-full-width.php' ) ) :

                remove_action( 'hainsworth_page',         'hainsworth\\page_header',         10 );

            endif;

        }

        public function timeline() {

            if ( is_page_template( 'templates/tpl-timeline.php' ) ) :

                remove_action( 'hainsworth_page',         'hainsworth\\page_header',         10 );

                add_action( 'hainsworth_page',            'hainsworth\\timeline',            50 );

            endif;

        }

    }

endif;