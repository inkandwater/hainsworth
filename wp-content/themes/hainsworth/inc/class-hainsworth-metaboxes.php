<?php

/**
 * Hainsworth Metaboxes Class
 *
 * @package hainsworth
 * @since   1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Hainsworth_Metaboxes' ) ) :

    /**
     * The Hainsworth Metaboxes class
     */
    class Hainsworth_Metaboxes {

        /**
         * The Text Domain of the theme.
         *
         * @since    1.0.0
         * @access   protected
         * @var      string    $text_domain    The Text Domain of the theme.
         */
        protected $text_domain;

        /**
         * The Meta_Prefix of the theme.
         *
         * @since    1.0.0
         * @access   protected
         * @var      string    $meta_prefix    The meta_prefix of the theme.
         */
        protected $meta_prefix;

        public function __construct() {

            $this->text_domain = 'hainsworth';
            $this->meta_prefix = '_hainsworth_';

        }

        public function metaboxes_init() {

            add_action( 'cmb2_admin_init',      array( $this, 'header' ),         10 );

        }

        public function header() {

            $header = new_cmb2_box( array(
                'id'            => $this->meta_prefix . 'header_block',
                'object_types'  => array( 'page' ),
                'show_names'    => true,
                'title'         => __( 'Header Block', $this->text_domain ),
            ) );

                $group = $header->add_field( array(
                    'description' => __( 'Creates a new slide for the header block', $this->text_domain ),
                    'id'          => $this->meta_prefix . 'header_block_slides',
                    'options'     => array(
                        'group_title'   => __( 'Slide {#}', $this->text_domain ),
                        'add_button'    => __( 'Add Another Slide', $this->text_domain ),
                        'remove_button' => __( 'Remove Slide', $this->text_domain ),
                        'sortable'      => true,
                        'closed'        => true,
                    ),
                    'type'        => 'group',
                ) );

                $header->add_group_field( $group, array(
                    'desc' => __( 'Add an image to the slide', $this->text_domain ),
                    'id'   => 'slide_background_image',
                    'name' => __( 'Slide Background Image', $this->text_domain ),
                    'type' => 'file',
                    'query_args' => array(
                        'type' => array(
                        	'image/gif',
                        	'image/jpeg',
                        	'image/png',
                        ),
                    ),
                    'preview_size' => array( 200, 80 ),
                ) );
                $header->add_group_field( $group, array(
                    'desc' => __( 'Add a title to the slide', $this->text_domain ),
                    'id'   => 'slide_title',
                    'name' => __( 'Slide Title', $this->text_domain ),
                    'type' => 'wysiwyg',
                    'options' => array(
                        'wpautop'       => true, // use wpautop?
                        'media_buttons' => false, // show insert/upload button(s)
                        'textarea_rows' => get_option('default_post_edit_rows', 3), // rows = "..."
                    ),
                ) );
                $header->add_group_field( $group, array(
                    'desc'    => __( 'Align the content', $this->text_domain ),
                    'id'      => 'slide_alignment',
                    'name'    => __( 'Slide Alignment', $this->text_domain ),
                    'type'    => 'select',
                    'default' => 'has-left-content',
                    'options' => array(
                        'has-left-content'   => 'Left',
                        'has-center-content' => 'Center',
                        'has-right-content'  => 'Right'
                    )
                ) );

        }

    }

endif;