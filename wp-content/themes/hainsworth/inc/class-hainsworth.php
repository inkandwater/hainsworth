<?php

/**
 * Set up Hainsworth theme
 *
 * @package hainsworth
 * @since   1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Hainsworth' ) ) :

    class Hainsworth {

        /**
         * The Text Domain of the theme.
         *
         * @since    2.1.0
         * @access   protected
         * @var      string    $text_domain    The Text Domain of the theme.
         */
        protected $text_domain;

        public function __construct() {

            $this->text_domain = 'hainsworth';

        }

        /**
         *
         * @since 1.0.0
         */
        public function init() {

            add_action( 'after_setup_theme',          array( $this, 'setup' ) );
            add_action( 'after_setup_theme',          array( $this, 'social_options' ) );
            add_action( 'after_setup_theme',          array( $this, 'cleanup' ) );
            add_action( 'after_setup_theme',          array( $this, 'register_menus' ) );
            add_action( 'widgets_init',               array( $this, 'register_widgets' ) );
            add_action( 'wp_enqueue_scripts',         array( $this, 'styles' ) );
            add_action( 'wp_enqueue_scripts',         array( $this, 'scripts' ) );

            add_filter( 'auto_update_theme',          array( $this, 'auto_update_settings' ), 10, 2 );
            add_action( 'enqueue_block_editor_assets',array( $this, 'block_assets' ), 99 );


        }

        /**
         * Sets up theme defaults and registers support for various WordPress features.
         *
         * @since  1.0.0
         */
        public function setup() {

            /**
             * Set the content width in pixels, based on the theme's design and stylesheet.
             */
            $GLOBALS['content_width'] = apply_filters( 'hainsworth_content_width', 1200 );

            /**
             * Load text domain
             */
            load_theme_textdomain( 'hainsworth', get_template_directory() . '/languages' );

            /**
             * Let WordPress manage the document title.
             */
            add_theme_support( 'title-tag' );

            /**
             * Declare support for selective refreshing of widgets.
             */
            add_theme_support( 'customize-selective-refresh-widgets' );

            /**
             * Enable support for Post Thumbnails on posts and pages.
             *
             * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
             */
            add_theme_support( 'post-thumbnails', array(
                'post',
                'page',
                'case-study'
            ) );

            add_theme_support( 'automatic-feed-links' );

            /**
             * Switch default core markup for search form, comment form, and comments
             * to output valid HTML5.
             */
            add_theme_support( 'html5', array(
                'search-form',
                'comment-form',
                'comment-list',
                'gallery',
                'caption'
            ) );

            /**
             * Enable support for Post Formats.
             * @see https://developer.wordpress.org/themes/functionality/post-formats/
             */
            add_theme_support( 'post-formats', array(
                'standard'
            ) );

            /**
             * Set up the WordPress core custom background feature.
             */
            $bg_defaults = array(
                'default-color'          => '',
                'default-repeat'         => 'no-repeat',
                'default-position-x'     => '',
                'default-attachment'     => '',
                'admin-head-callback'    => '',
                'admin-preview-callback' => ''
            );

            /**
             * Set up the WordPress core custom background feature.
             */
            add_theme_support( 'custom-background', $bg_defaults );

            /**
             * Set up the WordPress core custom logo feature.
             */
            add_theme_support( 'custom-logo', array(
                'height'      => 150,
                'width'       => 300,
                'flex-height' => true,
            ) );

            add_theme_support( 'disable-custom-font-sizes' );
            add_theme_support( 'disable-custom-colors' );
            add_theme_support( 'responsive-embeds' );
            add_theme_support( 'align-wide' );

            add_image_size( 'post' ,'800', '450', true );

        }

        /**
         * Turn off auto update on this theme to avoid
         * name conflicts.
         *
         * @since  2.1.0
         */
        public function auto_update_settings( $update, $item ) {

            // Array of theme slugs to never auto-update
            $themes = array (
                'hainsworth',
            );

            if ( in_array( $item->slug, $themes ) ) {
                return false; // never update themes in this array
            } else {
                return $update; // Else, use the normal API response to decide whether to update or not
            }

        }

        /**
         * Removes a bunch of default links, scripts and styles that
         * clutter up the <head> section.
         *
         * @since  2.1.0
         */
        public function cleanup() {

            remove_action( 'wp_head',         'rsd_link' );
            remove_action( 'wp_head',         'wlwmanifest_link' );
            remove_action( 'wp_head',         'wp_generator' );
            remove_action( 'wp_head',         'parent_post_rel_link' );
            remove_action( 'wp_head',         'start_post_rel_link' );
            remove_action( 'wp_head',         'index_rel_link' );
            remove_action( 'wp_head',         'adjacent_posts_rel_link' );
            remove_action( 'wp_head',         'adjacent_posts_rel_link_wp_head', 10, 0 );
            remove_action( 'wp_head',         'wp_shortlink_wp_head' );
            remove_action( 'wp_head',         'feed_links',                      2 );
            remove_action( 'wp_head',         'feed_links_extra',                3 );
            remove_action( 'wp_head',         'wp_shortlink_wp_head',            10, 0 );
            remove_action( 'wp_head',         'print_emoji_detection_script',    7 );
            remove_action( 'wp_print_styles', 'print_emoji_styles' );

        }

        /**
         * Registers menu locations for the theme.
         *
         * @since  1.0.0
         */
        public function register_menus() {

            register_nav_menus(
                array(
                    'main-menu'         => __( 'Main Menu', 'hainsworth' ),
                    'site-map'          => __( 'Footer Nav', 'hainsworth' ),
                    'product-menu'       => __( 'Product Menu', 'hainsworth' )
                )
            );

        }

        /**
         * Registers widget areas
         *
         * Footer widget areas can be filtered to change the
         * number (this also helps with column appearance).
         *
         * @since  1.0.0
         */
        public function register_widgets() {

            register_sidebar( array(
                "name"              => __( "Footer Widget Area", 'hainsworth' ),
                "id"                => 'footer-widget-area',
                'description'       => __( "Footer Widget Area", 'hainsworth' ),
                'before_widget'     => '<div id="%1$s" class="widget %2$s">',
                'after_widget'      => '</div>',
                'before_title'      => '<span class="widget-title">',
                'after_title'       => '</span>',
            ) );

            register_sidebar( array(
                "name"              => __( "Footer Bar", 'hainsworth' ),
                "id"                => 'footer-bar',
                'description'       => __( '', 'hainsworth' ),
                'before_widget'     => '<div id="%1$s" class="widget %2$s">',
                'after_widget'      => '</div>',
                'before_title'      => '<span class="widget-title">',
                'after_title'       => '</span>',
            ) );

            register_sidebar( array(
                "name"              => __( "Blog Sidebar", 'hainsworth' ),
                "id"                => 'blog-sidebar',
                'description'       => __( '', 'hainsworth' ),
                'before_widget'     => '<div id="%1$s" class="widget %2$s">',
                'after_widget'      => '</div>',
                'before_title'      => '<span class="widget-title">',
                'after_title'       => '</span>',
            ) );


            register_sidebar( array(
                "name"              => __( "Home Page Widget Area", 'hainsworth' ),
                "id"                => 'home-sidebar',
                'description'       => __( '', 'hainsworth' ),
                'before_widget'     => '<div id="%1$s" class="widget %2$s">',
                'after_widget'      => '</div>',
                'before_title'      => '<h2 class="widget-title section-title">',
                'after_title'       => '</h2>',
            ) );

        }

        public function social_options() {

            $social_links = array(
                'facebook'      => array(
                    'slug'             => 'facebook_link',
                    'class'            => 'fab fa-facebook',
                    'label'            => __( 'Facebook', 'hainsworth' )
                ),
                'instagram'     => array(
                    'slug'             => 'instagram_link',
                    'class'            => 'fab fa-instagram',
                    'label'            => __( 'Instagram', 'hainsworth' )
                ),
                'twitter'       => array(
                    'slug'             => 'twitter_link',
                    'class'            => 'fab fa-twitter',
                    'label'            => __( 'Twitter', 'hainsworth' )
                ),
                'youtube'      => array(
                    'slug'             => 'youtube_link',
                    'class'            => 'fab fa-youtube',
                    'label'            => __( 'YouTube', 'hainsworth' )
                )
            );

            update_option( 'social_media_links', $social_links );

        }

        /**
         * Registers and enqueues front end stylesheets
         *
         * @since  1.0.0
         */
        public function styles() {

            global $theme_version;

            wp_register_style(
                'hainsworth',
                get_stylesheet_uri(),
                '',
                $theme_version,
                'all'
            );

            wp_enqueue_style( 'hainsworth' );

            wp_register_style(
                'ninja-forms',
                get_template_directory_uri() . '/assets/css/ninja-forms.css',
                '',
                $theme_version,
                'all'
            );

            wp_enqueue_style( 'ninja-forms' );


        }

        /**
         * Registers and enqueues front end scripts
         *
         * @since  1.0.0
         */
        public function scripts() {

            wp_register_script(
                'main',
                get_template_directory_uri() . '/assets/js/dist/main.min.js',
                array( 'jquery', 'wp-dom' ),
                '',
                true
            );

            wp_enqueue_script( 'main' );

            wp_register_script(
                'carousel',
                get_template_directory_uri() . '/assets/js/dist/carousel.min.js',
                array( 'jquery' ),
                '',
                true
            );

            wp_enqueue_script( 'carousel' );

            wp_register_script(
                'cookies',
                get_template_directory_uri() . '/assets/js/dist/cookies.min.js',
                array( 'jquery' ),
                $theme_version,
                true
            );

            $show    = get_theme_mod( 'hainsworth_cookies_show' );
            $content = get_theme_mod( 'hainsworth_cookies_content' );

            if ( true == $show && !empty( $content ) ) :
                wp_enqueue_script( 'cookies' );
            endif;

        }

        /**
         *
         *
         * @since  1.0.0
         */
        public function block_assets() {

            global $theme_version;

            // Styles.
            wp_enqueue_style(
                'hainsworth-core-block-css', // Handle.
                get_theme_file_uri( '/assets/css/blocks.min.css' ),
                array( 'wp-edit-blocks' ), // Dependency to include the CSS after it.
                $theme_version,
                'all'
            );

        }

    }

endif;