<?php

/**
 * Hainsworth template tags.
 *
 * @package hainsworth
 * @since   1.0.0
 */

namespace hainsworth;

if ( ! function_exists( __NAMESPACE__ . '\site_title_or_logo' ) ) :

    /**
     * Display the site title or logo
     *
     * @param bool $echo Echo the string or return it.
     * @return string
     */
    function site_title_or_logo( $echo = true ) {

        if ( function_exists( 'the_custom_logo' ) && has_custom_logo() ) {

            $logo = get_custom_logo();
            $html = is_home() ? '<h1 class="logo">' . $logo . '</h1>' : $logo;

        } else {

            $tag = is_home() ? 'h1' : 'h1';

            $html = '<' . esc_attr( $tag ) . ' class="site-title"><a href="' . esc_url( home_url( '/' ) ) . '" rel="home">' . esc_html( get_bloginfo( 'name' ) ) . '</a></' . esc_attr( $tag ) .'>';

            if ( '' !== get_bloginfo( 'description' ) ) {
                $html .= '<p class="site-description">' . esc_html( get_bloginfo( 'description', 'display' ) ) . '</p>';
            }

        }

        if ( ! $echo ) {
            return $html;
        }

        echo $html;
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\main_menu' ) ) :

    function main_menu() {

        /**
        * Displays a navigation menu
        * @param array $args Arguments
        */
        $defaults = array(
            'theme_location'  => 'main-menu',
            'container'       => false,
            'menu_class'      => 'menu dropdown-menu',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'depth'           => 0,
            'walker'          => new \Hainsworth_Nav_Walker( 'main-navigation' )
        );

        wp_nav_menu( apply_filters( 'hainsworth_main_menu_args', $defaults ) );

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\site_map_menu' ) ) :

    function site_map_menu() {

        /**
        * Displays a navigation menu
        * @param array $args Arguments
        */
        $defaults = array(
            'theme_location'  => 'site-map',
            'container'       => false,
            'menu_class'      => 'menu dropdown-menu',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'depth'           => 0,
            'walker'          => ''
        );

        wp_nav_menu( apply_filters( 'hainsworth_main_menu_args', $defaults ) );

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\product_menu' ) ) :

    function product_menu() {

        /**
        * Displays a navigation menu
        * @param array $args Arguments
        */
        $defaults = array(
            'theme_location'  => 'product-menu',
            'container'       => false,
            'menu_class'      => 'menu',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'depth'           => 0,
            'walker'          => ''
        );

        wp_nav_menu( apply_filters( 'hainsworth_main_menu_args', $defaults ) );

    }

endif;