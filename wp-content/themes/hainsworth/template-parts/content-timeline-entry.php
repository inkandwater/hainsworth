<?php

/**
 * Template used for displaying page content in page.php
 *
 * @package hainsworth
 * @since  1.0.0
 */
global $post;

$featured = get_post_meta( $post->ID, 'hainsworth_timeline_featured_year', true );
?>

<!-- page-<?php the_ID(); ?> -->
<div id="page-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="post__content content-block">

        <?php the_content(); ?>

    </div>

    <?php if ( has_post_thumbnail() ) { ?>

        <div class="post__thumbnail entry-thumbnail">
            <?php the_post_thumbnail( 'full' ); ?>
        </div>

    <?php } ?>

    <?php if ( 'on' == $featured ) :
        the_title( '<span class="featured-year">', '</span>' );
    endif; ?>

</div>
<!-- /page-<?php the_ID(); ?> -->
