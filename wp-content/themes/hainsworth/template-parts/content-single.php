<?php

/**
 * Template used for displaying post content on single pages.
 *
 * @package hainsworth
 * @since  1.0.0
 */

?>

<!-- post-<?php the_ID(); ?> -->
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php
    /**
     * Functions hooked into hainsworth_single_post_top
     *
     */
    do_action( 'hainsworth_single_post_top' );

    /**
     * Functions hooked into hainsworth_single_post
     *
     * @see 10 hainsworth_post_header
     * @see 20 hainsworth_post_thumbnail
     * @see 30 hainsworth_post_content
     * @see 40 hainsworth_post_meta
     */
    do_action( 'hainsworth_single_post' );

    /**
     * Functions hooked in to hainsworth_single_post_bottom
     *
     * @see 10 hainsworth_display_comments
     */
    do_action( 'hainsworth_single_post_bottom' );
    ?>

</div>
<!-- /post-<?php the_ID(); ?> -->
