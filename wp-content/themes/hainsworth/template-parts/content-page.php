<?php

/**
 * Template used for displaying page content in page.php
 *
 * @package hainsworth
 * @since  1.0.0
 */

?>

<!-- page-<?php the_ID(); ?> -->
<div id="page-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php
    /**
     * Functions hooked into hainsworth_page_top
     *
     */
    do_action( 'hainsworth_page_top' );

    /**
     * Functions hooked into hainsworth_page
     *
     * @see 10 hainsworth_page_header
     * @see 20 hainsworth_page_content
     */
    do_action( 'hainsworth_page' );

    /**
     * Functions hooked in to hainsworth_page_bottom
     *
     */
    do_action( 'hainsworth_page_bottom' );
    ?>

</div>
<!-- /page-<?php the_ID(); ?> -->
