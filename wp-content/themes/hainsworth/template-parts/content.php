<?php

/**
 * Template used to display post content.
 *
 * @package hainsworth
 * @since  1.0.0
 */

?>

<!-- post-<?php the_ID(); ?> -->
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php
    /**
     * Functions hooked in to hainsworth_loop_post
     *
     * @see 10 hainsworth_post_header
     * @see 20 hainsworth_post_thumbnail
     * @see 30 hainsworth_post_content
     * @see 40 hainsworth_post_meta
     */
    do_action( 'hainsworth_loop_post' );
    ?>

</article>
<!-- /post-<?php the_ID(); ?> -->
