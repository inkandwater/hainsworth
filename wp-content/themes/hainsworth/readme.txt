=== Hainsworth ===

Contributors: tomnapier, paulrmyers
Requires at least: 4.4
Tested up to: 4.9.6
Stable tag: 2.1.2
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.html

== Description ==

Hainsworth is a lean parent theme using bourbon, neat and modular scale.

== Changelog ==
* 1.1.3 *
- Add no-js styles

* 1.1.2 *
- Fix product navigation issue

* 1.1.1 *
- Update hero slider styles so text is clearer.

* 1.1.0 *
- Add timer and modal block
- Fix gallery carousel for WP 5.6 compatibility
- Add 'gold' page template for a new colour scheme.

* 1.0.0 *
- Initial release