/**
 * dropdown-menus.js
 *
 * Handles toggling the navigation menu for small screens.
 *
 * The 'focus' class matches hover styles in the CSS.
 */
"use-strict";

var s,
    dropdown = {

        settings: {
            menuItems:     document.getElementsByClassName("main-navigation__menu-item--has-children")
        },

        init: function () {

            s = this.settings;
            if (null !== s.menuItems) {
                this.bindUIActions();
            }

        },

        bindUIActions: function () {

            for (var i = 0; i < s.menuItems.length; i++) {

                var child = s.menuItems[i].childNodes[0];

                child.addEventListener("click", function (event) {
                    if (window.innerWidth < 600 && document.body.classList.contains('js-menu-active')) {
                        event.preventDefault();
                        dropdown.active(this);
                    }
                });

            }

        },

        active: function (element) {

            // Setup the variables for each of the elements
            var parent      = element.parentNode;
            var activeClass = 'js-dropdown-active';
            var menuItems   = document.getElementsByClassName("main-navigation__menu-item--has-children");

            // If the parent of 'element' has the active class, remove it!
            if (parent.classList.contains(activeClass)) {
                parent.classList.remove(activeClass);
            } else {
                // For all elements remove the active class
                // then add it to the element selected
                for (var i = 0; i < menuItems.length; i++) {
                    menuItems[i].classList.remove(activeClass);
                }
                parent.classList.toggle(activeClass);
            }

            window.addEventListener('resize', function () {
                if (window.innerWidth < 600) {
                    parent.classList.remove(activeClass);
                }
            });

        }

    }