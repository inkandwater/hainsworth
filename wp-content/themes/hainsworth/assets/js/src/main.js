document.addEventListener( 'DOMContentLoaded', function() {
	navigation.init();
	dropdown.init();
});

if ( document.readyState !== 'loading' ) {
	document.body.classList.remove('no-js');
} else {
	document.addEventListener('DOMContentLoaded', function() {
	document.body.classList.remove('no-js');
	});
}