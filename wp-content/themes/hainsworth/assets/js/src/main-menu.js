/**
 * toggle.js
 *
 * Handles the department dropdown and the ajax requesting for
 * the reload of the section to display the selected department.
 *
 */
"use-strict";

var t,
    navigation = {

        settings: {
            toggle: document.getElementById( 'menu-toggle' ),
            body: document.getElementsByTagName( 'body' )[0],
            activeClass: 'js-menu-active'
        },

        init: function () {

            t = this.settings;

            if ( null !== t.toggle ) {
                this.bindUIActions();
            }

        },

        bindUIActions: function () {

            t.toggle.addEventListener( 'click', function() {
                navigation.activeClass();
            });

        },

        activeClass: function ( status ) {

            if ( t.body.classList.contains( t.activeClass ) ) {
                t.body.classList.remove( t.activeClass );
            } else if ( 'remove' === status ) {
                t.body.classList.remove( t.activeClass );
            } else {
                t.body.classList.add( t.activeClass );
            }

        }

    }