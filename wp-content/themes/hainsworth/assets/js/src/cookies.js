window.onload = function() {

    var cookieBar    = document.getElementById('cookie-bar');
    var cookiesClose = document.getElementById('close-cookie-bar');

    var cookie = getCookie( 'hainsworth-cookies' );

    if ( cookie == '' ) {
        cookieBar.classList.remove('cookie-set');
    } else {
        cookieBar.classList.add('cookie-set');
    }

    if ( cookiesClose !== null ) {

        cookiesClose.addEventListener( 'click', function(e) {

            e.preventDefault();
            setCookie( 'hainsworth-cookies', 'Set on: ' + setDate(), 7 );
            cookieBar.classList.add('cookie-set');

        });

    }

}


function setCookie( name, value, days ) {

    var expires = "";
    if ( days ) {
        var date = new Date();
        date.setTime( date.getTime() + ( days*24*60*60*1000 ) );
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";

}

function setDate() {

    var date = new Date();
    return date;

}

function getCookie(cname) {

    var name          = cname + "=";
    var decodedCookie = decodeURIComponent( document.cookie );
    var ca            = decodedCookie.split( ';' );

    for( var i = 0; i < ca.length; i++ ) {
        var c = ca[i];
        while ( c.charAt(0) == ' ' ) {
            c = c.substring(1);
        }
        if ( c.indexOf(name) == 0 ) {
            return c.substring( name.length, c.length );
        }
    }

    return "";

}