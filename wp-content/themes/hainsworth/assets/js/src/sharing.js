( function( $ ) {

    $( '.js-share' ).click( function( event ) {
      event.preventDefault();

      var link = $( this ).attr( 'href' )

      window.open(link, '_blank', 'height=700,width=500');
      return false;
    } );


  } )( jQuery );