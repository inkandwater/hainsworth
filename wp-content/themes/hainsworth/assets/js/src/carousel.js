( function( $ ) {

    $(document).ready(function(){

        $('.js-slider-init').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            fade: false,
            lazyLoad: 'progressive',
            speed: 500
        });
    });

} )( jQuery );