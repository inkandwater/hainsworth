/* Animations
=====================================
*/

(function($) {

    /**
     * Copyright 2012, Digital Fusion
     * Licensed under the MIT license.
     * http://teamdf.com/jquery-plugins/license/
     *
     * @author Sam Sehnert
     * @desc A small plugin that checks whether elements are within
     *     the user visible viewport of a web browser.
     *     only accounts for vertical position, not horizontal.
     */
    $.fn.visible = function(partial) {

        var $t            = $( this ),
            $w            = $( window ),
            viewTop       = $w.scrollTop(),
            viewBottom    = viewTop + $w.height(),
            _top          = $t.offset().top,
            _bottom       = _top + $t.height(),
            compareTop    = partial === true ? _bottom : _top,
            compareBottom = partial === true ? _top : _bottom;

        return ( ( compareBottom <= viewBottom ) && ( compareTop >= viewTop ) );

    };

    $(document).ready( function(){

        /**
         * @desc Adds class to the already visible elements
         */
        $('.animate').each( function( i, el ) {

            var el = $( el );

            if ( el.visible( true ) ) {
                console.log( el.attr('class') + ' is visible' );
                el.addClass( "animate--is-visible" );
            }

        });

    });

    /**
     * @desc Adds the animate--active class to each of the elements that
     *       come into view as you scroll the page.
     */
    $(window).scroll(function(e) {

        $('.animate').each( function( i, el ) {

            var el = $( el );

            if ( el.visible( true ) ) {
                $( this ).addClass( "animate--active" );
            }

        });

    });

})(jQuery);
