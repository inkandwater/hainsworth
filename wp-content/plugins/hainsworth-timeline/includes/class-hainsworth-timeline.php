<?php
/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Hainsworth_Timeline
 * @subpackage Hainsworth_Timeline/includes
 * @author     Paul Myers <paul@inkandwater.co.uk>
 */
class Hainsworth_Timeline {

    /**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      Hainsworth_Timeline_Loader    $loader    Maintains and registers all hooks for the plugin.
     */
    protected $loader;

    /**
     * The unique identifier of this plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $plugin_name    The string used to uniquely identify this plugin.
     */
    protected $plugin_name;

    /**
     * The current version of the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $version    The current version of the plugin.
     */
    protected $version;

    /**
     * The custom post type slug.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $version    The custom post type slug.
     */
    protected $post_type;

    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the admin area and
     * the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function __construct() {

        $this->plugin_name = 'hainsworth-timeline';
        $this->version     = '1.0.0';
        $this->post_type   = 'timeline';

        $this->load_dependencies();
        $this->set_locale();
        $this->define_admin_hooks();

    }

    /**
     * Load the required dependencies for this plugin.
     *
     * Include the following files that make up the plugin:
     *
     * - Hainsworth_Timeline_Loader. Orchestrates the hooks of the plugin.
     * - Hainsworth_Timeline_i18n. Defines internationalization functionality.
     * - Hainsworth_Timeline_Admin. Defines all hooks for the admin area.
     * - Hainsworth_Timeline_Public. Defines all hooks for the public side of the site.
     *
     * Create an instance of the loader which will be used to register the hooks
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function load_dependencies() {

        /**
         * The class responsible for orchestrating the actions and filters of the
         * core plugin.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-hainsworth-timeline-loader.php';

        /**
         * The class responsible for defining internationalization functionality
         * of the plugin.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-hainsworth-timeline-i18n.php';

        /*
         * Class responsible for defining the custom post type
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-hainsworth-timeline-post-types.php';

        /**
         * The class responsible for defining all actions that occur in the admin area.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-hainsworth-timeline-admin.php';

        $this->loader = new Hainsworth_Timeline_Loader();

    }

    /**
     * Define the locale for this plugin for internationalization.
     *
     * Uses the Hainsworth_Timeline_i18n class in order to set the domain and to register the hook
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function set_locale() {

        $plugin_i18n = new Hainsworth_Timeline_i18n();

        $this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

    }

    /**
     * Register all of the hooks related to the admin area functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_admin_hooks() {

        $plugin_admin = new Hainsworth_Timeline_Admin( $this->get_plugin_name(), $this->get_version(), $this->get_post_type() );

        $this->loader->add_action( 'after_setup_theme', $plugin_admin, 'setup', 10 );
        $this->loader->add_action( 'cmb2_admin_init', $plugin_admin, 'metaboxes', 10 );

        $this->loader->add_filter( 'manage_edit-' . $this->post_type . '_columns', $plugin_admin, 'set_custom_admin_columns', 10, 1 );
        $this->loader->add_filter( 'manage_'. $this->post_type . '_posts_custom_column', $plugin_admin, 'custom_admin_columns', 10, 2 );

        $plugin_post_type = new Hainsworth_Timeline_Post_Types( $this->get_plugin_name(), $this->get_version(), $this->get_post_type() );

        $this->loader->add_action( 'init', $plugin_post_type, 'register' );

    }


    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @since    1.0.0
     */
    public function run() {
        $this->loader->run();
    }

    /**
     * The name of the plugin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     *
     * @since     1.0.0
     * @return    string    The name of the plugin.
     */
    public function get_plugin_name() {
        return $this->plugin_name;
    }

    /**
     * The reference to the class that orchestrates the hooks with the plugin.
     *
     * @since     1.0.0
     * @return    Hainsworth_Timeline_Loader    Orchestrates the hooks of the plugin.
     */
    public function get_loader() {
        return $this->loader;
    }

    /**
     * Retrieve the version number of the plugin.
     *
     * @since     1.0.0
     * @return    string    The version number of the plugin.
     */
    public function get_version() {
        return $this->version;
    }

    /**
     * Retrieve the custom post type slug.
     *
     * @since     1.0.0
     * @return    string    Retrieve the custom post type slug.
     */
    public function get_post_type() {
        return $this->post_type;
    }

}