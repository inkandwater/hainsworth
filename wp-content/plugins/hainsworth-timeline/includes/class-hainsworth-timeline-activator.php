<?php
/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Hainsworth_Timeline
 * @subpackage Hainsworth_Timeline/includes
 * @author     Paul Myers <paul@inkandwater.co.uk>
 */
class Hainsworth_Timeline_Activator {

    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    1.0.0
     */
    public static function activate() {

        Hainsworth_Timeline_Post_Types::register();

        flush_rewrite_rules();

    }

}