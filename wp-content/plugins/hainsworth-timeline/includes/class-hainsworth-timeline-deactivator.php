<?php
/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Hainsworth_Timeline
 * @subpackage Hainsworth_Timeline/includes
 * @author     Paul Myers <paul@inkandwater.co.uk>
 */
class Hainsworth_Timeline_Deactivator {

    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    1.0.0
     */
    public static function deactivate() {

    }

}
