<?php
/**
 * The custom post types that the plugin adds.
 *
 * @package    Hainsworth_Timeline
 * @subpackage Hainsworth_Timeline/public
 * @author     Paul Myers <paul@inkandwater.co.uk>
 */
class Hainsworth_Timeline_Post_Types {

    /**
     * Register Post Type. (use period)
     *
     * Register the custom post type for this plugin.
     *
     * @since    1.0.0
     */
    public static function register() {

        $labels = array(
            'name'                  => _x( 'Timeline', 'Post Type General Name', 'hainsworth-timeline' ),
            'singular_name'         => _x( 'Timeline', 'Post Type Singular Name', 'hainsworth-timeline' ),
            'menu_name'             => __( 'Timeline', 'hainsworth-timeline' ),
            'name_admin_bar'        => __( 'Timeline', 'hainsworth-timeline' ),
            'archives'              => __( 'Timeline Archives', 'hainsworth-timeline' ),
            'parent_item_colon'     => __( 'Timeline Entry:', 'hainsworth-timeline' ),
            'all_items'             => __( 'All Timeline Entries', 'hainsworth-timeline' ),
            'add_new_item'          => __( 'Add New Entry', 'hainsworth-timeline' ),
            'add_new'               => __( 'Add New', 'hainsworth-timeline' ),
            'new_item'              => __( 'New Entry', 'hainsworth-timeline' ),
            'edit_item'             => __( 'Edit Entry', 'hainsworth-timeline' ),
            'update_item'           => __( 'Update Entry', 'hainsworth-timeline' ),
            'view_item'             => __( 'View Entry', 'hainsworth-timeline' ),
            'search_items'          => __( 'Search Timeline Entries', 'hainsworth-timeline' ),
            'not_found'             => __( 'Not found', 'hainsworth-timeline' ),
            'not_found_in_trash'    => __( 'Not found in Trash', 'hainsworth-timeline' ),
            'insert_into_item'      => __( 'Insert into Timeline', 'hainsworth-timeline' ),
            'uploaded_to_this_item' => __( 'Uploaded to this Timeline Entry', 'hainsworth-timeline' ),
            'items_list'            => __( 'Timeline list', 'hainsworth-timeline' ),
            'items_list_navigation' => __( 'Timeline list navigation', 'hainsworth-timeline' ),
            'filter_items_list'      => __( 'Filter Timeline list', 'hainsworth-timeline' ),
            'featured_image'        => __( 'Featured Image', 'hainsworth-timeline' ),
            'set_featured_image'    => __( 'Set Featured Image', 'hainsworth-timeline' ),
            'remove_featured_image' => __( 'Remove Featured Image', 'hainsworth-timeline' ),
            'use_featured_image'    => __( 'Use as Featured Image', 'hainsworth-timeline' ),

        );

        $Timeline_args = array(
            'label'                 => __( 'Timeline', 'hainsworth-timeline' ),
            'labels'                => $labels,
            'supports'              => array(
                'title',
                'thumbnail',
                'editor',
                'page-attributes'
            ),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 50,
            'menu_icon'             => 'dashicons-share',
            'show_in_nav_menus'     => false,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => true,
            'publicly_queryable'    => false,
            'capability_type'       => 'post',
            'show_in_rest'          => true
        );

        register_post_type(
            'Timeline',
            apply_filters( 'hainsworth_timeline_post_type_args', $Timeline_args )
        );

    }

}