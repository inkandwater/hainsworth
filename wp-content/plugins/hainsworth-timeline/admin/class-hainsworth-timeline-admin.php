<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Hainsworth_Timeline
 * @subpackage Hainsworth_Timeline/admin
 * @author     Paul Myers <paul@inkandwater.co.uk>
 */
class Hainsworth_Timeline_Admin {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct( $plugin_name, $version, $post_type ) {

        $this->plugin_name = $plugin_name;
        $this->version     = $version;
        $this->post_type   = $post_type;

    }

    public function setup() {

        add_image_size( 'timeline', 500, 300, true );
        add_image_size( 'timeline-small', 120, 60, true );

        $supportedTypes = get_theme_support( 'post-thumbnails' );

        if( $supportedTypes === false )
            add_theme_support( 'post-thumbnails', array( $this->post_type ) );
        elseif( is_array( $supportedTypes ) ) {
            $supportedTypes[0][] = $this->post_type;
            add_theme_support( 'post-thumbnails', $supportedTypes[0] );
        }

    }

    public function metaboxes() {

        $timeline = new_cmb2_box( array(
            'id'            => 'hainsworth_timeline_metaboxes',
            'title'         => __( 'Featured Year', 'hainsworth-timeline' ),
            'object_types'  => array( 'timeline' ),
            'context'       => 'side'
        ) );

            $timeline->add_field( array(
                'name'       => __( 'Add as Featured Year', 'hainsworth-timeline' ),
                'id'         => 'hainsworth_timeline_featured_year',
                'type'       => 'checkbox',
                'desc'       => __( 'Add this post as a featured year in the timeline', 'hainsworth-timeline' )
            ) );

    }

    public function set_custom_admin_columns( $columns ) {

        $columns = array(
            'cb'             => '<input type="checkbox" />',
            'title'          => __( 'Timeline Entry', $this->plugin_name ),
            'featured-image' => __( 'Featured Image', $this->plugin_name ),
            'date'           => __( 'Date Added', $this->plugin_name )
        );

        return $columns;

    }

    public function custom_admin_columns( $column, $post_id ) {

        global $post;

        switch ( $column ) {

            case 'featured-image' :

                if( has_post_thumbnail() ) :
                    echo the_post_thumbnail( 'timeline-small' );
                else :
                    _e( '--', $this->plugin_name );
                endif;

            break;

        }

    }


}