<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://inkandwater.co.uk
 * @since             1.0.0
 * @package           Hainsworth_Timeline
 *
 * @wordpress-plugin
 * Plugin Name:       Hainsworth Timeline
 * Plugin URI:        http://inkandwater.co.uk/
 * Description:       Adds 'Timeline' post type.
 * Version:           1.0.0
 * Author:            Paul Myers | Ink & Water
 * Author URI:        http://inkandwater.co.uk/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       hainsworth-timeline
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-hainsworth-timeline-activator.php
 */
function activate_hainsworth_timeline() {
    require_once plugin_dir_path( __FILE__ ) . 'includes/class-hainsworth-timeline-post-types.php';
    require_once plugin_dir_path( __FILE__ ) . 'includes/class-hainsworth-timeline-activator.php';

    Hainsworth_Timeline_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-hainsworth-timeline-deactivator.php
 */
function deactivate_hainsworth_timeline() {
    require_once plugin_dir_path( __FILE__ ) . 'includes/class-hainsworth-timeline-deactivator.php';

    Hainsworth_Timeline_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_hainsworth_timeline' );
register_deactivation_hook( __FILE__, 'deactivate_hainsworth_timeline' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-hainsworth-timeline.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_hainsworth_timeline() {

    $plugin = new Hainsworth_Timeline();
    $plugin->run();

}

run_hainsworth_timeline();