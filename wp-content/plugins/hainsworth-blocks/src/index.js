/**
 * Gutenberg Blocks
 *
 * All blocks related JavaScript files should be imported here.
 * You can create a new block folder in this dir and include code
 * for that block here as well.
 *
 * All blocks should be included here since this is the file that
 * Webpack is compiling as the input file.
 */

import './blocks/download/index.js';
import './blocks/lead-paragraph/index.js';
import './blocks/swatch-gallery/index.js';
import './blocks/slider-gallery/index.js';
import './blocks/image-text/index.js';
import './blocks/modal/index.js';
import './blocks/countdown-timer/index.js';

wp.domReady( () => {

    // Remove default button styles
    wp.blocks.unregisterBlockStyle( 'core/button', 'squared' );
    wp.blocks.unregisterBlockStyle( 'core/button', 'default' );
    wp.blocks.unregisterBlockStyle( 'core/button', 'outline' );

    wp.blocks.unregisterBlockType( 'core/verse' );
    wp.blocks.unregisterBlockType( 'core/preformatted' );
    wp.blocks.unregisterBlockType( 'core/code' );
    wp.blocks.unregisterBlockType( 'core/audio' );
    wp.blocks.unregisterBlockType( 'core/more' );

});

// Add in our own styles that match the themes' BEM button classes
wp.blocks.registerBlockStyle( 'core/button', {
    name: 'button-default',
    label: 'Hollow (Default)',
    isDefault: true
} );

wp.blocks.registerBlockStyle( 'core/button', {
    name: 'button-hollow-white',
    label: 'Hollow White',
} );

wp.blocks.registerBlockStyle( 'core/cover', {
    name: 'cover-hero',
    label: 'Site Hero'
} );

wp.blocks.registerBlockStyle( 'core/cover', {
    name: 'cover-default',
    label: 'Default',
    isDefault: true
} );

wp.blocks.registerBlockStyle( 'core/gallery', {
    name: 'slider',
    label: 'Slider'
} );

wp.blocks.registerBlockStyle( 'core/file', {
    name: 'no-icons',
    label: 'No Icons',
    isDefault: true
} );

wp.blocks.registerBlockStyle( 'core/file', {
    name: 'icons',
    label: 'All Icons'
} );

wp.blocks.registerBlockStyle( 'core/file', {
    name: 'icon-pdf',
    label: 'PDF Only'
} );

wp.blocks.registerBlockStyle( 'core/file', {
    name: 'icon-ftp',
    label: 'FTP Icon'
} );

wp.blocks.registerBlockStyle( 'core/file', {
    name: 'icon-washing-machine',
    label: 'Washing Machine Icon'
} );

wp.blocks.registerBlockStyle( 'core/file', {
    name: 'icon-uv',
    label: 'UV Protection Icon'
} );

wp.blocks.registerBlockStyle( 'core/file', {
    name: 'icon-ftp-washing-machine',
    label: 'FTP & Washing Machine Icon'
} );

wp.blocks.registerBlockStyle( 'core/file', {
    name: 'icon-ftp-uv',
    label: 'FTP & UV Icon'
} );

wp.blocks.registerBlockStyle( 'core/file', {
    name: 'icon-washing-machine-uv',
    label: 'Washing Machine & UV Icon'
} );