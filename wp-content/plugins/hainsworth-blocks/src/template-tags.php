<?php
if ( ! function_exists( 'file_block_icon_filter' ) ) :

    function file_block_icon_filter( $block_content, $block ) {

        if( "core/file" !== $block['blockName'] ) {
            return $block_content;
        }

        if( isset( $block['attrs']['nofollow'] ) && $block['attrs']['nofollow'] != false ) :

            $to_insert = 'rel="noreferrer noopener nofollow"';
            $block_content = str_replace( 'rel="noreferrer noopener"', $to_insert, $block_content );

        endif;

    return $block_content;

    }

endif;