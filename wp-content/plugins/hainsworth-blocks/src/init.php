<?php
/**
 * Blocks Initializer
 *
 * Enqueue CSS/JS of all the blocks.
 *
 * @since   1.0.0
 * @package CGB
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

add_filter( 'render_block',     'file_block_icon_filter',             10, 3 );

/**
 * Enqueue Gutenberg block assets for backend editor.
 *
 * @uses {wp-blocks} for block type registration & related functions.
 * @uses {wp-element} for WP Element abstraction — structure of blocks.
 * @uses {wp-i18n} to internationalize the block's text.
 * @uses {wp-editor} for WP editor styles.
 * @since 1.0.0
 */
function hainsworth_blocks_cgb_editor_assets() { // phpcs:ignore

    // Scripts.
    wp_enqueue_script(
        'hainsworth_blocks-cgb-block-js', // Handle.
        plugins_url( '/build/index.js', dirname( __FILE__ ) ), // Block.build.js: We register the block here. Built with Webpack.
        array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', 'wp-dom' ), // Dependencies, defined above.
        filemtime( plugin_dir_path( __DIR__ ) . 'build/index.js' ), // Version: File modification time.
        true // Enqueue the script in the footer.
    );

    // Styles.
    wp_enqueue_style(
        'hainsworth_blocks-cgb-block-editor-css', // Handle.
        plugins_url( 'build/css/build.editor.css', dirname( __FILE__ ) ), // Block editor CSS.
        array( 'wp-edit-blocks' ) // Dependency to include the CSS after it.
        // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.editor.build.css' ) // Version: File modification time.
    );

}

// Hook: Editor assets.
add_action( 'enqueue_block_editor_assets', 'hainsworth_blocks_cgb_editor_assets' );

/**
 * Load vendor scripts for the carousel and lightbox
 *
 * @since 1.0.0
 */
function hainsworth_enqueue_scripts() {

    wp_enqueue_script(
        'swatch-gallery-js',
        plugins_url( 'build/swatch-gallery/swatch-gallery.min.js', dirname( __FILE__ ) ),
        array( 'wp-dom' ),
        true
    );

    wp_enqueue_style(
        'swatch-gallery-css',
        plugins_url( 'build/swatch-gallery/swatch-gallery.min.css', dirname( __FILE__ ) ),
        array(),
        false,
        'all'
    );

    wp_enqueue_script(
        'slider-gallery-js',
        plugins_url( 'build/slider-gallery/slider-gallery.min.js', dirname( __FILE__ ) ),
        array( 'jquery' ),
        true
    );

    if ( has_block( "hainsworth/modal" ) ) {
        wp_enqueue_script(
            'modal',
            plugins_url( 'build/modal.min.js', dirname( __FILE__ ) ),
            array( 'jquery' ),
            true
        );
    }

    if ( has_block( "hainsworth/countdown-timer" ) ) {
        wp_enqueue_script(
            'countdown-timer',
            plugins_url( 'build/countdown-timer.min.js', dirname( __FILE__ ) ),
            array( 'jquery' ),
            true
        );
    }

    wp_enqueue_style(
        'slider-gallery-css',
        plugins_url( 'build/slider-gallery/slider-gallery.min.css', dirname( __FILE__ ) ),
        array(),
        false,
        'all'
    );

}

add_action( 'wp_enqueue_scripts', 'hainsworth_enqueue_scripts' );