import './editor.scss';

const { Fragment } = wp.element;
const { __, } = wp.i18n;
const {
  IconButton,
  Toolbar,
} = wp.components;
const {
  BlockControls,
  MediaUpload,
  MediaPlaceholder,
} = wp.editor;

const {
  registerBlockType
} = wp.blocks;

import PhotoSwipe from 'PhotoSwipe';
import PhotoSwipeUI_Default from 'PhotoSwipe';

const ALLOWED_MEDIA_TYPES = [ 'image' ];

registerBlockType( 'hainsworth/swatch-gallery', {
  title: __( 'Swatch Gallery' ), // Block title.
  icon: 'format-gallery', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
  category: 'common', // Block category
  keywords: [ //Keywords
    __('photos'),
    __('images')
  ],
  attributes: { //Attributes
    images : { //Images array
      type: 'array',
    }
  },
  supports: {
    align: true
  },

  /**
   * The edit function describes the structure of your block in the context of the editor.
   * This represents what the editor will render when the block is used.
   *
   * The "edit" property must be a valid function.
   *
   * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
   */
  edit({ attributes, className, setAttributes }) {

    //Destructuring the images array attribute
    const {images = []} = attributes;

    //Displays the images
    const displayImages = (images) => {
      return (
          images.map( (image) => {

              const imgCaption = image.caption;

              return (
                <figure class="swatch-gallery__swatch">
                  <img class="swatch-gallery__image" src={image.sizes.thumbnail.url} itemprop="thumbnail" alt={image.alt} title={imgCaption} />
                </figure>
              )
          })
      )
    }

    //If there are no images, display the MediaPlaceholder
    if ( images.length === 0 ) {

      //JSX to return
      return (
        <div>
          <Fragment>
            <MediaPlaceholder
              icon="format-gallery"
              labels={ {
                title: __( 'Swatch Gallery' ),
                instructions: __( 'Drag images, upload new ones or select files from your library.' ),
              } }
              onSelect={(media) => {
                setAttributes({images: media}); }
              }
              accept="image/*"
              multiple
            />
          </Fragment>
        </div>
      );

    }

    return (
      <Fragment>
        <BlockControls>
          { !! images.length && (
            <Toolbar>
              <MediaUpload
                onSelect={(media) => { setAttributes({images: media}); } }
                allowedTypes={ ALLOWED_MEDIA_TYPES }
                multiple
                gallery
                value={ images.map( ( img ) => img.id ) }
                render={ ( { open } ) => (
                  <IconButton
                    className="components-toolbar__control"
                    label={ __( 'Edit Gallery' ) }
                    icon="edit"
                    onClick={ open }
                  />
                ) }
              />
            </Toolbar>
          ) }
        </BlockControls>
        <div class="wp-block-hainsworth-swatch-gallery">
          {displayImages(images)}
        </div>
      </Fragment>

    )

  },

  /**
   * The save function defines the way in which the different attributes should be combined
   * into the final markup, which is then serialized by Gutenberg into post_content.
   *
   * The "save" property must be specified and must be a valid function.
   *
   * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
   */
  save( { attributes } ) {

    const { images = [] } = attributes;

    // Displays the images
    const displayImages = (images) => {
      return (
        images.map( (image,index) => {

          const imgCaption = image.caption;

          return (

            <figure class="swatch-gallery__swatch" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
              <a class="swatch-gallery__link" href={image.sizes.large.url} data-size={image.sizes.large.width + 'x' + image.sizes.large.height} data-caption={ imgCaption }>
                  <img class="swatch-gallery__image" src={image.sizes.thumbnail.url} itemprop="thumbnail" alt={image.alt} />
              </a>
            </figure>

          )
        })
      )

    }

    //JSX to return
    return (
      <Fragment>
        <div class="wp-block-hainsworth-swatch-gallery">
          { displayImages(images) }
        </div>
        <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

              <div class="pswp__bg"></div>
              <div class="pswp__scroll-wrap">
                  <div class="pswp__container">
                      <div class="pswp__item"></div>
                      <div class="pswp__item"></div>
                      <div class="pswp__item"></div>
                  </div>
                  <div class="pswp__ui pswp__ui--hidden">
                      <div class="pswp__top-bar">
                          <div class="pswp__counter"></div>
                          <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                          <button class="pswp__button pswp__button--share" title="Share"></button>
                          <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                          <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                          <div class="pswp__preloader">
                              <div class="pswp__preloader__icn">
                                <div class="pswp__preloader__cut">
                                  <div class="pswp__preloader__donut"></div>
                                </div>
                              </div>
                          </div>
                      </div>

                      <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                          <div class="pswp__share-tooltip"></div>
                      </div>

                      <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
                      </button>

                      <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                      </button>

                      <div class="pswp__caption">
                          <div class="pswp__caption__center"></div>
                      </div>

                  </div>

              </div>
        </div>

      </Fragment>
    );

  }

} );