//  Import CSS.
import './editor.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType, createBlock } = wp.blocks; // Import registerBlockType() from wp.blocks
const { Fragment }          = wp.element;
const {
  RichText,
  AlignmentToolbar,
  BlockControls,
} = wp.blockEditor;

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'hainsworth/lead-paragraph', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Lead Paragraph' ), // Block title.
	icon: 'editor-paragraph', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'Lead Paragraph' ),
	],
	supports: {
		multiple: false,
	},
	attributes: {
		content: {
			type:     'array',
			source:   'children',
			selector: 'p',
			default:  [],
        },
        placeholder: {
            default: __( 'Add a lead paragraph' )
        },
		textAlignment: {
			type: 'string',
		}
	},
	transforms: {
		from: [
			{
				type: 'block',
				blocks: [ 'core/paragraph' ],
				transform: ( { content } ) => {
					return createBlock( 'hainsworth/lead-paragraph', {
						content,
					} );
				},
			},
		],
		to: [
			{
				type: 'block',
				blocks: [ 'core/paragraph' ],
				transform: ( { content } ) => {
					return createBlock( 'core/paragraph', {
						content,
					} );
				},
			},
		],
	},
	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	edit: function( props ) {

		const {
			attributes,
			setAttributes
		} = props;

		const {
			align,
			content,
			placeholder
		} = attributes;

		// Event handler to update the value of the content when changed in editor.
		const onChangeContent = value => {
			props.setAttributes( { content: value } );
		};

		return (
			<Fragment>
				<BlockControls>
					<AlignmentToolbar
						value    = { attributes.align }
						onChange={ ( nextAlign ) => {
							setAttributes( { align: nextAlign } );
						} }
					/>
				</BlockControls>
				<RichText
					className   = { 'hainsworth-lead-paragraph' }
					tagName     = "p"
					placeholder = { props.attributes.placeholder }
					onChange={ ( nextContent ) => {
						setAttributes( {
							content: nextContent,
						} );
					} }
					style       = { { textAlign: attributes.align } }
					value       = { props.attributes.content }
				/>
			</Fragment>
		);
	},

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	save: function( props ) {
		return (
			<RichText.Content
				tagName = "p"
				class   = "lead-paragraph"
				value   = { props.attributes.content }
			/>
		);
	},
} );
