( function( $ ) {

  $(document).ready(function(){

      $('.is-style-slider .blocks-gallery-grid').slick({
          nextArrow:'<button type="button" class="slick-next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 96 20"><path d="M0,10H95.42" style="fill:none;stroke:#d51d55;stroke-width:2px"/><path d="M87,19.31,95.42,10,87,.68" style="fill:none;stroke:#d51d55;stroke-width:2px"/></svg></button>',
          prevArrow:'<button type="button" class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 96 20"><title>right-arrow</title><path d="M96,10H.56" style="fill:none;stroke:#d51d55;stroke-width:2px"/><path d="M9,.68.57,10,9,19.31" style="fill:none;stroke:#d51d55;stroke-width:2px"/></svg></button>',
          infinite: true,
          fade: false,
          autoplay: true,
          autoplaySpeed: 2000,
          slidesToShow: 2,
          slidesToScroll: 1,
          responsive: [
              {
                breakpoint: 850,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                }
              }
          ]
      });

  });

} )( jQuery );
