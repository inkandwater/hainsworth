export default class timer {

  constructor() {

      this.settings = {

          timer: document.querySelector( '#wp-block-hainsworth-countdown-timer' )

      }

  }

  init() {

      let s = this.settings;
      this.bindUIActions();
      this.setTimer(s.timer.dataset.datetime);

  }

  bindUIActions() {

      let s = this.settings;

  }

  setTimer(endtime) {

      const countDownDate = new Date(endtime).getTime();

      const time = setInterval(function() {

        // Get today's date and time
        const now = new Date().getTime();

        // Find the distance between now and the count down date
        const distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        let days = Math.floor(distance / (1000 * 60 * 60 * 24));
        let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        let seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Display the result in the element with id="demo"
        if ( days >= 0 ) {
            document.getElementsByClassName( 'wp-block-hainsworth-countdown-timer__days' )[0].innerHTML = days;
        } else {
            document.getElementsByClassName( 'wp-block-hainsworth-countdown-timer__days' )[0].parentElement.classList.add("disabled");
        }

        if ( hours >= 0 ) {
            document.getElementsByClassName( 'wp-block-hainsworth-countdown-timer__hours' )[0].innerHTML = hours;
        } else if ( hours < 0 && minutes < 0 && seconds < 0 ) {
            document.getElementsByClassName( 'wp-block-hainsworth-countdown-timer__hours' )[0].parentElement.classList.add("disabled");
        }

        if ( minutes >= 0 ) {
            document.getElementsByClassName( 'wp-block-hainsworth-countdown-timer__minutes' )[0].innerHTML = minutes;
        } else if ( hours < 0 && minutes < 0 && seconds < 0 ) {
            document.getElementsByClassName( 'wp-block-hainsworth-countdown-timer__minutes' )[0].parentElement.classList.add("disabled");
        }

        if ( seconds >= 0 ) {
             document.getElementsByClassName( 'wp-block-hainsworth-countdown-timer__seconds' )[0].innerHTML = seconds;
        } else if ( hours < 0 && minutes < 0 && seconds < 0 ) {
            document.getElementsByClassName( 'wp-block-hainsworth-countdown-timer__seconds' )[0].parentElement.classList.add("disabled");
        }

        // If the count down is finished, write some text
        if (distance < 0) {
            clearInterval(time);
        }

      });

  }

}