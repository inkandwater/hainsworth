// Import Attributes
import blockAttributes from './attributes';

const { __ } = wp.i18n;
const { registerBlockType, getBlockDefaultClassName } = wp.blocks;
const { InspectorControls } = wp.blockEditor;
const { Fragment } = wp.element;
const { PanelBody, PanelRow, DateTimePicker } = wp.components;

registerBlockType( 'hainsworth/countdown-timer', {

	title: __( 'Countdown Timer' ),
	icon: 'clock',
    category: 'common',
    supports: {
        multiple: false
    },
	attributes: blockAttributes,

	edit( { className, setAttributes, attributes } ) {

        const { datetime } = attributes;
        const blockClass = getBlockDefaultClassName( 'hainsworth/countdown-timer' );

        const onUpdateDate = ( dateTime ) => {
            var newDateTime = moment(dateTime).format( 'YYYY-MM-DDTHH:mm' );
            setAttributes( { datetime: newDateTime } );
        };

        const getTimeRemaining = (endtime) => {

            var t = Date.parse(endtime) - Date.parse(new Date());
            var seconds = Math.floor( (t/1000) % 60 );
            var minutes = Math.floor( (t/1000/60) % 60 );
            var hours = Math.floor( (t/(1000*60*60)) % 24 );
            var days = Math.floor( t/(1000*60*60*24) );

            return (
              <Fragment>
                <table>
                  <tr>
                    <th class={ blockClass + "__days h4" }>{ days }</th>
                    <th class={ blockClass + "__hours h4" }>{ hours }</th>
                    <th class={ blockClass + "__minutes h4" }>{ minutes }</th>
                    <th class={ blockClass + "__seconds h4" }>{ seconds }</th>
                  </tr>
                  <tr>
                    <td>{ "days" }</td>
                    <td>{ "hours" }</td>
                    <td>{ "minutes" }</td>
                    <td>{ "seconds" }</td>
                  </tr>
                </table>
              </Fragment>
            );

        };

        const controls = (
            <InspectorControls>
                <PanelBody
                    title={ "Expiry Date & Time" }
                    icon={ <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M9 11H7v2h2v-2zm4 0h-2v2h2v-2zm4 0h-2v2h2v-2zm2-7h-1V2h-2v2H8V2H6v2H5c-1.11 0-1.99.9-1.99 2L3 20c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 16H5V9h14v11z"/><path d="M0 0h24v24H0z" fill="none"/></svg> }
                    initialOpen={ true }>
                    <PanelRow>
                        <DateTimePicker
                            currentDate={ datetime }
                            onChange={ ( val ) => onUpdateDate( val ) }
                            is12Hour={ true } />
                    </PanelRow>
                </PanelBody>
            </InspectorControls>
        );

		return (
        <div className={ className }>
            { controls }
            { getTimeRemaining( datetime ) }
        </div>
		);
	},

	// No information saved to the block
	// Data is saved to post meta via attributes
	save( { className, attributes } ) {

        const { datetime } = attributes;
        const blockClass = getBlockDefaultClassName( 'hainsworth/countdown-timer' );

        return (
            <div id={ blockClass } className={ className } data-datetime={ datetime }>

                <div className={ blockClass + "__time" }>
                    <h3 className={ blockClass + "__days mega" }></h3>
                    <h4 className={ blockClass + "__heading h5" }>{ "days" }</h4>
                </div>
                <div className={ blockClass + "__time" }>
                    <h3 className={ blockClass + "__hours mega" }></h3>
                    <h4 className={ blockClass + "__heading h5" }>{ "hours" }</h4>
                </div>
                <div className={ blockClass + "__time" }>
                    <h3 className={ blockClass + "__minutes mega" }></h3>
                    <h4 className={ blockClass + "__heading h5" }>{ "minutes" }</h4>
                </div>
                <div className={ blockClass + "__time" }>
                    <h3 className={ blockClass + "__seconds mega" }></h3>
                    <h4 className={ blockClass + "__heading h5" }>{ "seconds" }</h4>
                </div>

            </div>
        );
	},
} );