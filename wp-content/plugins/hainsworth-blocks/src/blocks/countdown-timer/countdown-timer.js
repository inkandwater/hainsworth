import timer from './front-end/countdown-timer.js';

( function( $ ) {

    $( document ).ready( function() {

        let countdownTimer = new timer();
        countdownTimer.init();

    });

})( jQuery );