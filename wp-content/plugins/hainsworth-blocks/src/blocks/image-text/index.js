import './editor.scss';

import classnames from 'classnames';

const { Fragment } = wp.element;
const { __ } = wp.i18n;
const {
    AlignmentToolbar,
    BlockControls,
    InnerBlocks,
    MediaPlaceholder,
    MediaUpload,
    RichText,
    InspectorControls
} = wp.blockEditor;

const { registerBlockType } = wp.blocks;
const { IconButton, Toolbar, PanelBody, ToggleControl } = wp.components;

const ALLOWED_BLOCKS = [
    'core/paragraph', 'core/list', 'core/file', 'core/image', 'core/button', 'core/heading', 'hainsworth/lead-paragraph'
];

registerBlockType( 'hainsworth/image-text', {
    title: __( 'Image & Text' ), // Block title.
    icon: <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M13 17h8v-2h-8v2zM3 19h8V5H3v14zM13 9h8V7h-8v2zm0 4h8v-2h-8v2z" /></svg>,
    category: 'common', // Block category
    keywords: [ //Keywords
        __('Image'),
        __('Text')
    ],
    attributes: {
		imgURL: {
            type: 'string',
            source: 'attribute',
            attribute: 'src',
            selector: 'img',
        },
        imgID: {
            type: 'number',
        },
        imgAlt: {
            type: 'string',
            source: 'attribute',
            attribute: 'alt',
            selector: 'img',
        },
        imgCaption: {
            type: 'string',
            source: 'html',
            selector: 'figcaption',
        },
        alignment: {
            type: 'string',
        },
        imagePosition: {
            type: 'string',
            default: 'left',
        },
        isStackedOnMobile: {
            type: 'boolean',
            default: true,
        },
	},
    supports: {
        align: [ 'wide' ],
    },

    /**
     * The edit function describes the structure of your block in the context of the editor.
     * This represents what the editor will render when the block is used.
     *
     * The "edit" property must be a valid function.
     *
     * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
     */
    edit({ attributes, className, setAttributes, isSelected }) {

        const { imgID, imgURL, imgAlt, imgCaption, alignment, isStackedOnMobile, imagePosition } = attributes;

        const onSelectImage = img => {
            setAttributes( {
                imgID:  img.id,
                imgURL: img.url,
                imgAlt: img.alt,
                imgCaption: img.caption
            } );
        };
        const onRemoveImage = () => {
            setAttributes({
                imgID: null,
                imgURL: null,
                imgAlt: null,
                imgCaption: null
            });
        }

        const onChangeAlignment = ( newAlignment ) => {
            setAttributes( { alignment: newAlignment })
        }

        const classNames = classnames( className, {
            'has-media-on-the-right': 'right' === imagePosition,
            'is-stacked-on-mobile': isStackedOnMobile,
        });

        const toolbarControls = [ {
			icon: 'align-pull-left',
			title: __( 'Show media on left' ),
			isActive: imagePosition === 'left',
			onClick: () => setAttributes( { imagePosition: 'left' } ),
		}, {
			icon: 'align-pull-right',
			title: __( 'Show media on right' ),
			isActive: imagePosition === 'right',
			onClick: () => setAttributes( { imagePosition: 'right' } ),
		} ];

        const controls = (
            <BlockControls>
                <Toolbar>
                    <MediaUpload
                        onSelect={ onSelectImage }
                        type="image"
                        render={ ( { open } ) => (
                            <IconButton
                                className="components-toolbar__control"
                                label={ __( 'Edit image' ) }
                                icon="edit"
                                onClick={ open }
                            />
                        ) }
                    />
                    { isSelected && imgID ? (
                        <IconButton
                            className="components-toolbar__control"
                            label={ __( 'Remove image' ) }
                            icon="no"
                            onClick={ onRemoveImage }
                        />
                    ) : null }
                </Toolbar>
                <Toolbar
                    controls={ toolbarControls }
                />
                <AlignmentToolbar
                    value={ alignment }
                    onChange={ onChangeAlignment }
                />
            </BlockControls>
        );

        return (
            <Fragment>
                <InspectorControls>
                    <PanelBody title={ __( 'Image & Text Settings' ) }>
                        <ToggleControl
                            label={ __( 'Stack on mobile' ) }
                            checked={ isStackedOnMobile }
                            onChange={ () => setAttributes( {
                                isStackedOnMobile: ! isStackedOnMobile,
                            } ) }
                        />
                    </PanelBody>
                    <PanelBody title={ __( 'Image Caption Alignment' ) }>
                        <AlignmentToolbar
                            value={ alignment }
                            onChange={ onChangeAlignment }
                        />
                    </PanelBody>
                </InspectorControls>

                <div className={ classNames }>
                    { controls }
                    { imgID ? (
                        <figure class="image-text__image-wrapper">
                            <img
                                src={ imgURL }
                                alt={ imgAlt }
                            />
                            <RichText
                                tagName     = "figcaption"
                                placeholder = { __( 'Write caption…' ) }
                                value       = { imgCaption }
                                onChange    = { ( imgCaption ) => setAttributes( { imgCaption: imgCaption } ) }
                                inlineToolbar
                                style={ { textAlign: alignment } }
                            />
                        </figure>
                    ) : (
                        <MediaPlaceholder
                            icon='format-image'
                            className={ 'image-text__image-wrapper' }
                            labels={ {
                                title: __( 'Image' ),
                                name: __( 'an image' ),
                            } }
                            onSelect={ onSelectImage }
                        />
                    )}
                    <div className={ 'image-text__text-wrapper' }>
                        <InnerBlocks
                            allowedBlocks={ ALLOWED_BLOCKS }
                        />
                    </div>
                </div>

            </Fragment>
		);

    },

    /**
     * The save function defines the way in which the different attributes should be combined
     * into the final markup, which is then serialized by Gutenberg into post_content.
     *
     * The "save" property must be specified and must be a valid function.
     *
     * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
     */
    save( { attributes } ) {

        const { imgID, imgURL, imgAlt, imgCaption, className, alignment, isStackedOnMobile, imagePosition } = attributes;

        const classNames = classnames( className, {
            'has-media-on-the-right': 'right' === imagePosition,
            'is-stacked-on-mobile': isStackedOnMobile,
        });

        return (
            <div className={ classNames }>
                <figure className="image-text__image-wrapper">
                    <img
                        src={ imgURL }
                        alt={ imgAlt }
                        id={ imgID }
                    />
                    <RichText.Content
                        tagName = "figcaption"
                        value   = { imgCaption }
                        style   = { { textAlign: alignment } }
                    />
                </figure>
                <div className={ 'image-text__text-wrapper' }>
                    <InnerBlocks.Content />
                </div>
            </div>
		);

    }

} );