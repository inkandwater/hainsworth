/**
 * WordPress Dependencies
 */
import { __ } from '@wordpress/i18n';
import { addFilter } from '@wordpress/hooks';
import { Fragment }  from '@wordpress/element';
import { InspectorControls } from '@wordpress/blockEditor';
import { createHigherOrderComponent } from '@wordpress/compose';
import { CheckboxControl, PanelBody } from '@wordpress/components';

//restrict to specific block names
const allowedBlocks = [ 'core/file' ];

/**
 * Add custom attribute for mobile visibility.
 *
 * @param {Object} settings Settings for the block.
 *
 * @return {Object} settings Modified settings.
 */
function addAttributes( settings ) {

  //check if object exists for old Gutenberg version compatibility
  //add allowedBlocks restriction
  if( typeof settings.attributes !== 'undefined' && allowedBlocks.includes( settings.name ) ){

    settings.attributes = Object.assign( settings.attributes, {
      nofollow: {
        type: 'boolean',
        default: false
      },
    });

  }

  return settings;
}

/**
 * Add mobile visibility controls on Advanced Block Panel.
 *
 * @param {function} BlockEdit Block edit component.
 *
 * @return {function} BlockEdit Modified block edit component.
 */
const withAdvancedControls = createHigherOrderComponent( ( BlockEdit ) => {
  return ( props ) => {

    const {
      name,
      attributes,
      setAttributes,
      isSelected,
    } = props;

    const { nofollow } = attributes;

    const setChecked = bool => {
      setAttributes({ nofollow: bool });
    };

    return (
      <Fragment>
        <BlockEdit {...props} />

        { isSelected && allowedBlocks.includes( name ) &&
          <InspectorControls>
            <PanelBody
                title={ __( 'Additional' ) }
                initialOpen={ true }>
                  <CheckboxControl
                      label="Add nofollow?"
                      checked={ nofollow }
                      onChange={ setChecked }
                  />
            </PanelBody>
          </InspectorControls>
        }

      </Fragment>
    );
  };
}, 'withAdvancedControls');

//add filters
addFilter(
  'blocks.registerBlockType',
  'hainsworth/custom-attributes',
  addAttributes
);

addFilter(
  'editor.BlockEdit',
  'hainsworth/custom-advanced-control',
  withAdvancedControls
);