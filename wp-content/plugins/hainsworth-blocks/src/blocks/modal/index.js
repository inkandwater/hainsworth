import classnames from 'classnames';
import "./editor.scss";

const { __ } = wp.i18n;
const { registerBlockType, getBlockDefaultClassName } = wp.blocks;

const {
    InnerBlocks,
    InspectorControls
} = wp.blockEditor;

const { PanelBody, ToggleControl, RadioControl } = wp.components;

registerBlockType( 'hainsworth/modal', {
    title: __( 'Modal' ),
    icon: 'video-alt3',
    category: 'common',
    supports: {
        multiple: false,
    },
    attributes: {
        fullscreen: {
            type: 'boolean',
            default: false
        },
        delay: {
            type: 'string',
            default: '1000'
        },
        theme: {
            type: 'string',
            default: 'light'
        }
    },

    edit( { className, setAttributes, attributes: { fullscreen, delay, theme } } ) {

        const classes = classnames( className, {
            'is-fullscreen' : fullscreen,
            [ `has-${ theme }-theme` ]: theme
        });

        const updateDelay = ( value ) => {
            setAttributes( { delay: value } );
        };

        const updateTheme = ( value ) => {
            setAttributes( { theme: value } );
        };

        const blockControls = (
            <InspectorControls>
                <PanelBody title={ __( 'Settings' ) }>
                    <ToggleControl
                        label={ __( 'Fullscreen Mode' ) }
                        checked={ fullscreen }
                        onChange={ () => setAttributes( {
                            fullscreen: ! fullscreen,
                        } ) }
                    />
                    <RadioControl
                        label    = { __( 'Loading Delay' ) }
                        help     = { __( 'Adds a preset amount of delay on the loading of the modal.' ) }
                        selected = { delay }
                        options  = { [
                            { label: 'No Delay', value: '0' },
                            { label: 'Short',    value: '1000' },
                            { label: 'Medium',   value: '3000' },
                            { label: 'Long',     value: '6000' },
                        ] }
                        onChange = { updateDelay }
                    />
                    <RadioControl
                        label    = { __( 'Modal Theme' ) }
                        help     = { __( 'Select between a light and dark theme.' ) }
                        selected = { theme }
                        options  = { [
                            { label: 'Light',  value: 'light' },
                            { label: 'Dark',   value: 'dark' },
                        ] }
                        onChange = { updateTheme }
                    />
                </PanelBody>
            </InspectorControls>
        );

        return (
            <div className={ classes }>
                { blockControls }
                <InnerBlocks />
            </div>
        );
    },

    // No information saved to the block
    // Data is saved to post meta via attributes
    save( { attributes: { fullscreen, delay, theme } } ) {

        const blockClass = getBlockDefaultClassName( 'hainsworth/modal' );

        const classes = classnames( blockClass, {
            'is-fullscreen' : fullscreen,
            [ `has-${ theme }-theme` ]: theme
        });

        return (
            <div className={ classes } data-delay={ delay } aria-hidden={ "false" }>
                <div className={ blockClass + "__inner-container" }>
                    <button id={ blockClass + "__close-modal" } className={ blockClass + "__close-modal" }>
                        <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/><path d="M0 0h24v24H0z" fill="none"/></svg>
                    </button>
                    <InnerBlocks.Content />
                </div>
            </div>
        );
    },
} );
