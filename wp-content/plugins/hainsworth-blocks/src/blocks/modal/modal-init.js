import modal from './front-end/front-end.js';

( function( $ ) {

    $( document ).ready( function() {

        let theModal = new modal();
        theModal.init();

    });

})( jQuery );