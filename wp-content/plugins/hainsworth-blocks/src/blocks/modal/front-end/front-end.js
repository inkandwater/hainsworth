export default class modal {

  constructor() {

      this.settings = {

          modal: document.getElementsByClassName( 'wp-block-hainsworth-modal' )[0],
          body: document.querySelector( 'body' ),
          modalCloseBtn: document.getElementsByClassName( 'wp-block-hainsworth-modal__close-modal' )[0],
          modalActive : 'modal-active',
          bodyActive : 'has-active-modal',

      }

    }

    init() {

        s = this.settings;

        if ( null !== s.modalCloseBtn ) {
            this.bindUIActions();
        }

    }

    bindUIActions() {

        s = this.settings;

        this.activeClass();

        s.modalCloseBtn.addEventListener( 'click', () => {
            this.removeClass();
        });

        // Close modal and stop video when ESC is pressed
        s.body.addEventListener( 'keydown', event => {

            if ( 'Escape' == event.code ) {
                this.removeClass();
            }

        });

    }

    removeClass() {

        s = this.settings;

        if ( s.modal.classList.contains( s.modalActive ) && s.body.classList.contains( s.bodyActive ) ) {
            s.modal.classList.remove( s.modalActive );
            s.body.classList.remove( s.bodyActive );
        }

    }

    activeClass() {

        s = this.settings;

        let delay = s.modal.dataset.delay;

        setTimeout( function() {

            if ( ! document.getElementsByClassName( 'wp-block-hainsworth-modal' )[0].classList.contains( 'modal-active' ) ) {
                document.getElementsByClassName( 'wp-block-hainsworth-modal' )[0].classList.add( 'modal-active' );
                document.querySelector( 'body' ).classList.add( 'has-active-modal' );
            }

        }, delay );



    }

}