'use strict';

/* --------------------------
# Dependencies
---------------------------*/

var gulp         = require( "gulp" ),
    autoprefixer  = require( "autoprefixer" ),
    notify       = require( "gulp-notify" ),
    concat       = require( "gulp-concat" ),
    uglify       = require( "gulp-uglify" ),
    rename       = require( "gulp-rename" ),
    sass         = require( "gulp-sass" ),
    postcss      = require( 'gulp-postcss' );

/* --------------------------
# Configuration
---------------------------*/

var config = {
    npmDir:   './node_modules/',
    jsPath:   './src/',
}

/* --------------------------
# Scripts Compile
---------------------------*/

gulp.task( 'slider', () => {

    var jsScripts = [
        config.npmDir + 'slick-carousel/slick/slick.min.js',
        config.jsPath + 'blocks/slider-gallery/front-end/slick-init.js',
    ];

    return gulp.src( jsScripts )
        .pipe( concat( 'slider-gallery.min.js' ) )
        .pipe( uglify() )
        .pipe( gulp.dest( './build/slider-gallery' ) )
        .pipe( notify( { message: 'Finished minifying Vendor JavaScript'} ) );

});

gulp.task( 'swatch-gallery', () => {

    var jsScripts = [
        config.npmDir + 'photoswipe/dist/photoswipe.js',
        config.npmDir + 'photoswipe/dist/photoswipe-ui-default.js',
        config.jsPath + '/blocks/swatch-gallery/front-end/photoswipe-init.js',
    ];

    return gulp.src( jsScripts )
        .pipe( concat( 'swatch-gallery.min.js' ) )
        .pipe( uglify() )
        .pipe( gulp.dest( './build/swatch-gallery' ) )
        .pipe( notify( { message: 'Finished minifying Vendor JavaScript'} ) );

});

gulp.task( 'countdown-timer', () => {

    var jsScripts = [
        config.jsPath + '/blocks/countdown-timer/front-end/countdown-timer.js',
    ];

    return gulp.src( jsScripts )
        .pipe( concat( 'countdown-timer.min.js' ) )
        .pipe( uglify() )
        .pipe( gulp.dest( './build/countdown-timer' ) )
        .pipe( notify( { message: 'Finished minifying Vendor JavaScript'} ) );

});

gulp.task( 'swatch-gallery-css', () => {

    return gulp.src( './src/blocks/swatch-gallery/front-end/photoswipe.scss' )
      .pipe( sass({
        outputStyle: 'compressed',
        includePaths: [
            config.npmDir + 'photoswipe/src/css/',
        ]
      }))
      .on("error", notify.onError(function (error) {
        return "Error: " + error.message;
      }))
      .pipe( rename( 'swatch-gallery.min.css' ) )
      .pipe( postcss([ autoprefixer() ]) )
      .pipe( gulp.dest( './build/swatch-gallery/' ) );


});

gulp.task( 'photoswipe-assets', function() { 
    return gulp.src( config.npmDir + '/photoswipe/src/css/default-skin/**.*' ) 
      .pipe( gulp.dest('./build/swatch-gallery/photoswipe/') ); 
});

/* --------------------------
# Slider
---------------------------*/

gulp.task( 'slider-gallery-css', () => {

    return gulp.src( './src/blocks/slider-gallery/front-end/slider.scss' )
      .pipe( sass({
        outputStyle: 'compressed',
        includePaths: [
            config.npmDir + '/slick-carousel/slick/',
        ]
      }))
      .on("error", notify.onError(function (error) {
        return "Error: " + error.message;
      }))
      .pipe( rename( 'slider-gallery.min.css' ) )
      //.pipe( postcss([ autoprefixer( {grid: true} ) ]) )
      .pipe( gulp.dest( './build/slider-gallery/' ) );


});

gulp.task( 'fonts', () => {
    return gulp.src( config.npmDir + '/slick-carousel/slick/fonts/**.*' )
      .pipe( gulp.dest('./build/slider-gallery/fonts/') );
});

gulp.task( 'loader', () => {
    return gulp.src( config.npmDir + '/slick-carousel/slick/ajax-loader.gif' )
      .pipe( gulp.dest('./build/slider-gallery/images') );
});

/* --------------------------
# Dependencies
---------------------------*/

gulp.task( 'build', gulp.series([ 'slider', 'slider-gallery-css', 'countdown-timer', 'swatch-gallery', 'swatch-gallery-css', 'photoswipe-assets', 'fonts', 'loader' ]) );