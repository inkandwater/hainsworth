const webpack = require("webpack");
const defaultConfig = require("@wordpress/scripts/config/webpack.config");
const postcssPresetEnv = require("postcss-preset-env");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserJSPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CopyPlugin     = require('copy-webpack-plugin');

module.exports = {
    ...defaultConfig,
    module: {
        ...defaultConfig.module,
        rules: [
            ...defaultConfig.module.rules,
            {
            test: /\.scss$/,
            use: [
              {
                  loader: MiniCssExtractPlugin.loader,
              },
              {
                  loader: "css-loader"
              },
              {
                  loader: "sass-loader"
              },
              {
                  loader: "postcss-loader",
                  options: {
                      ident: "postcss",
                      plugins: () => [postcssPresetEnv({ autoprefixer: { grid: true, flexbox: true } })]
                  }
              }
          ],
          },
        ]
    },
    plugins: [

        ...defaultConfig.plugins,

        new MiniCssExtractPlugin({
          filename: 'css/build.editor.css',
          ignoreOrder: false,
        })

    ],
    optimization: {
        minimizer: [new TerserJSPlugin({}), new OptimizeCSSAssetsPlugin({})],
    },
};