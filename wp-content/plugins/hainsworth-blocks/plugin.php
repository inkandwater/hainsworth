<?php
/**
 * Plugin Name: Hainsworth Blocks
 * Plugin URI: https://www.inkandwater.co.uk
 * Description: Custom Blocks and Block Extensions for Hainsworth
 * Author: Ink & Water Ltd
 * Author URI: https://inkandwater.co.uk/
 * Version: 1.1.0
 * License: GPL2+
 * License URI: https://www.gnu.org/licenses/gpl-2.0.txt
 *
 * @package CGB
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Block Initializer.
 */
require_once plugin_dir_path( __FILE__ ) . 'src/init.php';
require_once plugin_dir_path( __FILE__ ) . 'src/template-tags.php';
