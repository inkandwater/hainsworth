=== SPERI Publications ===
Contributors: tom-napier
Donate link: http://inkandwater.co.uk
Requires at least: 4.0
Tested up to: 4.5.1
Stable tag: 4.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Adds 'Publications' post type and a type taxonomy.

== Changelog ==