<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://inkandwater.co.uk
 * @since             1.0.0
 * @package           Hainsworth_Case_Studies
 *
 * @wordpress-plugin
 * Plugin Name:       Hainsworth Case Studies
 * Plugin URI:        http://inkandwater.co.uk/
 * Description:       Adds 'Case Studies' post type.
 * Version:           1.0.0
 * Author:            Paul Myers | Ink & Water
 * Author URI:        http://inkandwater.co.uk/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       hainsworth-case-studies
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-hainsworth-case-studies-activator.php
 */
function activate_hainsworth_case_studies() {
    require_once plugin_dir_path( __FILE__ ) . 'includes/class-hainsworth-case-studies-post-types.php';
    require_once plugin_dir_path( __FILE__ ) . 'includes/class-hainsworth-case-studies-activator.php';

    Hainsworth_Case_Studies_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-hainsworth-case-studies-deactivator.php
 */
function deactivate_hainsworth_case_studies() {
    require_once plugin_dir_path( __FILE__ ) . 'includes/class-hainsworth-case-studies-deactivator.php';

    Hainsworth_Case_Studies_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_hainsworth_case_studies' );
register_deactivation_hook( __FILE__, 'deactivate_hainsworth_case_studies' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-hainsworth-case-studies.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_hainsworth_case_studies() {

    $plugin = new Hainsworth_Case_Studies();
    $plugin->run();

}

run_hainsworth_case_studies();