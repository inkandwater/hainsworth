<?php
/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Hainsworth_Case_Studies
 * @subpackage Hainsworth_Case_Studies/includes
 * @author     Paul Myers <paul@inkandwater.co.uk>
 */
class Hainsworth_Case_Studies_Activator {

    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    1.0.0
     */
    public static function activate() {

        Hainsworth_Case_Studies_Post_Types::register();

        flush_rewrite_rules();

    }

}
