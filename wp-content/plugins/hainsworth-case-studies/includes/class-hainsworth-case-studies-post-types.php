<?php
/**
 * The custom post types that the plugin adds.
 *
 * @package    Hainsworth_Case_Studies
 * @subpackage Hainsworth_Case_Studies/public
 * @author     Paul Myers <paul@inkandwater.co.uk>
 */
class Hainsworth_Case_Studies_Post_Types {

    /**
     * Register Post Type. (use period)
     *
     * Register the custom post type for this plugin.
     *
     * @since    1.0.0
     */
    public static function register() {

        $labels = array(
            'name'                  => _x( 'Case Studies', 'Post Type General Name', 'hainsworth-case-studies' ),
            'singular_name'         => _x( 'Case Study', 'Post Type Singular Name', 'hainsworth-case-studies' ),
            'menu_name'             => __( 'Case Studies', 'hainsworth-case-studies' ),
            'name_admin_bar'        => __( 'Case Studies', 'hainsworth-case-studies' ),
            'archives'              => __( 'Case Studies Archives', 'hainsworth-case-studies' ),
            'parent_item_colon'     => __( 'Parent Case Study:', 'hainsworth-case-studies' ),
            'all_items'             => __( 'All Case Studies', 'hainsworth-case-studies' ),
            'add_new_item'          => __( 'Add New Case Study', 'hainsworth-case-studies' ),
            'add_new'               => __( 'Add New', 'hainsworth-case-studies' ),
            'new_item'              => __( 'New Case Study', 'hainsworth-case-studies' ),
            'edit_item'             => __( 'Edit Case Study', 'hainsworth-case-studies' ),
            'update_item'           => __( 'Update Case Study', 'hainsworth-case-studies' ),
            'view_item'             => __( 'View Case Study', 'hainsworth-case-studies' ),
            'search_items'          => __( 'Search Case Studies', 'hainsworth-case-studies' ),
            'not_found'             => __( 'Not found', 'hainsworth-case-studies' ),
            'not_found_in_trash'    => __( 'Not found in Trash', 'hainsworth-case-studies' ),
            'insert_into_item'      => __( 'Insert into Case Studies', 'hainsworth-case-studies' ),
            'uploaded_to_this_item' => __( 'Uploaded to this Case Study', 'hainsworth-case-studies' ),
            'items_list'            => __( 'Case Studies list', 'hainsworth-case-studies' ),
            'items_list_navigation' => __( 'Case Studies list navigation', 'hainsworth-case-studies' ),
            'filter_items_list'      => __( 'Filter Case Studies list', 'hainsworth-case-studies' ),
            'featured_image'        => __( 'Featured Image', 'hainsworth-case-studies' ),
            'set_featured_image'    => __( 'Set Featured Image', 'hainsworth-case-studies' ),
            'remove_featured_image' => __( 'Remove Featured Image', 'hainsworth-case-studies' ),
            'use_featured_image'    => __( 'Use as Featured Image', 'hainsworth-case-studies' ),

        );

        $case_studies_args = array(
            'label'                 => __( 'Case Studies', 'hainsworth-case-studies' ),
            'labels'                => $labels,
            'supports'              => array(
                'title',
                'editor',
                'thumbnail'
            ),
            'hierarchical'          => true,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'menu_icon'             => 'dashicons-media-document',
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => true,
            'exclude_from_search'   => true,
            'publicly_queryable'    => true,
            'capability_type'       => 'post',
            'show_in_rest'          => true
        );

        register_post_type( 'case-study', apply_filters( 'hainsworth_case_studies_post_type_args', $case_studies_args ) );

    }

}