<?php
/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Hainsworth_Case_Studies
 * @subpackage Hainsworth_Case_Studies/includes
 * @author     Paul Myers <paul@inkandwater.co.uk>
 */
class Hainsworth_Case_Studies_i18n {


    /**
     * Load the plugin text domain for translation.
     *
     * @since    1.0.0
     */
    public function load_plugin_textdomain() {

        load_plugin_textdomain(
            'hainsworth-case-studies',
            false,
            dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
        );

    }



}
